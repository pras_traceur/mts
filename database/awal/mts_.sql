-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 21 Apr 2019 pada 20.32
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mts_`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `catatan`
--

CREATE TABLE `catatan` (
  `id_catatan` int(11) NOT NULL,
  `id_tahun` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_siswa` int(11) DEFAULT NULL,
  `keterangan` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `catatan`
--

INSERT INTO `catatan` (`id_catatan`, `id_tahun`, `id_kelas`, `id_siswa`, `keterangan`) VALUES
(1, 1, 55, 1, 'Tingkatkan terus belajarnya');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ekstrakurikuler`
--

CREATE TABLE `ekstrakurikuler` (
  `idEkskul` int(10) NOT NULL,
  `ekskul` varchar(100) NOT NULL,
  `pembimbing` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ekstrakurikuler`
--

INSERT INTO `ekstrakurikuler` (`idEkskul`, `ekskul`, `pembimbing`, `username`) VALUES
(15, 'Tartil Quran 4', 'Hj. Jumi Ainiyatun', 'admin'),
(16, 'Teknologi Informasi dan Komunikasi 	', 'Nur Habibah, S.Pd', 'admin'),
(17, 'Pramuka', 'Ely Miftahurrahmah, S.E.', 'admin'),
(18, 'OSIS', 'Biba', 'admin'),
(19, 'PMR (Palang Merah Remaja)', 'Habibi ', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `id_guru` int(5) NOT NULL,
  `id_users` int(11) NOT NULL,
  `nama_guru` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `nip` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `alamat` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `tempat_lahir` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `tgl_lahir` text COLLATE latin1_general_ci NOT NULL,
  `hp` text COLLATE latin1_general_ci NOT NULL,
  `jk` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `jabatan` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `aktif` varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`id_guru`, `id_users`, `nama_guru`, `nip`, `alamat`, `tempat_lahir`, `tgl_lahir`, `hp`, `jk`, `jabatan`, `aktif`) VALUES
(675, 7, 'Moh. Alimun, SE., ME.', '55555', 'Sumberwudi', 'Lamongan', '09 januari 1899', '081999953924', 'Laki-Laki', 'Guru', 'Y'),
(678, 8, 'Drs. H. Moh Musa', '44444', 'Kalanganyar karanggeneng Lamongan', 'Lamongan', '09 januari 1899', '081999953924', 'Laki-Laki', 'Guru', 'Y'),
(679, 9, 'Mustajab, S.Pd.', '33333', 'Glagah Lamongan', 'Lamongan', '09 januari 1899', '081999953924', 'Laki-Laki', 'Guru', 'Y'),
(680, 6, 'Agus Sugatot, M.Pd.', '22222', 'Manyar Gresik', 'Gresik', '09 januari 1899', '081999953924', 'Laki-Laki', 'Guru', 'Y'),
(681, 24, 'Moh. Syahidan, S.Pd.', '66666', 'Sumenep', 'Sumenep', '21 Agustus 1996', '081999953924', 'Laki-Laki', 'Guru', 'Y'),
(682, 10, 'ASMAUR ROHMAN, S.Pd.', '23234', 'Kalanganyar karanggeneng Lamongan', 'Lamongan', '09 januari 1899', '081999953924', 'Laki-Laki', 'Guru', 'Y'),
(683, 11, 'ABD. BASIR SADZALI, S.Pd.', '23454', 'Kalanganyar karanggeneng Lamongan', 'Lamongan', '09 januari 1899', '081999953924', 'Laki-Laki', 'Guru', 'Y'),
(684, 12, 'CHOIRUL HADI, S.Ag.', '34222', 'Kalanganyar karanggeneng Lamongan', 'Lamongan', '09 januari 1899', '081999953924', 'Laki-Laki', 'Guru', 'Y'),
(685, 13, 'ALI IRFANI', '234329', 'Kalanganyar karanggeneng Lamongan', 'Lamongan', '09 januari 1899', '081999953924', 'Laki-Laki', 'Guru', 'Y'),
(686, 14, 'SHOHIHAH, S.Pd.', '234327', 'Kalanganyar karanggeneng Lamongan', 'Lamongan', '09 januari 1899', '081999953924', 'Laki-Laki', 'Guru', 'Y'),
(687, 15, 'ULYATUL MABRUROH, S.Pd.', '4345', 'Kalanganyar karanggeneng Lamongan', 'Lamongan', '09 januari 1899', '081999953924', 'Laki-Laki', 'Guru', 'Y'),
(688, 16, 'MAHSULI ARIF, S.Ag.', '54654', 'Kalanganyar karanggeneng Lamongan', 'Lamongan', '09 januari 1899', '081999953924', 'Laki-Laki', 'Guru', 'Y'),
(689, 17, 'ABD. RASYID S.Pd.', '234433', 'Kalanganyar karanggeneng Lamongan', 'Lamongan', '09 januari 1899', '081999953924', 'Laki-Laki', 'Guru', 'Y'),
(690, 18, 'Muzayyanah, S.Pd.', '21234', 'Kalanganyar karanggeneng Lamongan', 'Lamongan', '09 januari 1899', '081999953924', 'Laki-Laki', 'Guru', 'Y'),
(691, 19, 'Ifatun Ni mah, S.Pd.', '34233', 'Kalanganyar karanggeneng Lamongan', 'Lamongan', '09 januari 1899', '081999953924', 'Perempuan', 'Guru', 'Y'),
(692, 20, 'Sonhaji, S.Pd.', '2122', 'Kalanganyar karanggeneng Lamongan', 'Lamongan', '09 januari 1899', '081999953924', 'Laki-Laki', 'Guru', 'Y'),
(693, 21, 'Junaidi, S.Pd.', '1254', 'Kalanganyar karanggeneng Lamongan', 'Lamongan', '09 januari 1899', '081999953924', 'Perempuan', 'Guru', 'Y'),
(694, 22, 'Harmaji, S.Pd', '8744', 'Kalanganyar karanggeneng Lamongan', 'Lamongan', '09 januari 1899', '081999953924', 'Laki-Laki', 'Guru', 'Y'),
(695, 23, 'Habibah', '12134', 'Kalanganyar karanggeneng Lamongan', 'Lamongan', '09 januari 1899', '081999953924', 'Perempuan', 'Guru', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `identitas`
--

CREATE TABLE `identitas` (
  `id_identitas` int(5) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nds` varchar(100) NOT NULL,
  `npsn` varchar(100) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `kode_pos` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_telp` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `desa` varchar(100) NOT NULL,
  `kecamatan` varchar(100) NOT NULL,
  `kabupaten` varchar(100) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `identitas`
--

INSERT INTO `identitas` (`id_identitas`, `nama`, `nds`, `npsn`, `alamat`, `kode_pos`, `email`, `no_telp`, `desa`, `kecamatan`, `kabupaten`, `provinsi`, `website`, `username`) VALUES
(1, 'MTs At Taqwa', '3333333333SDSD', 'SSSSSSSSSSSSS', 'Lamongan', '69466', 'email@sekolah@gmail.com', '081999953924', 'Lamongan', 'Lamongan', 'Lamongan', 'Jawa Timur', 'www.attaqwa.sch.id', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(5) NOT NULL,
  `nama_kelas` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `aktif` varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `nama_kelas`, `username`, `aktif`) VALUES
(56, 'IX', 'admin', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelompok`
--

CREATE TABLE `kelompok` (
  `id_kelompok` int(11) NOT NULL,
  `id_siswa` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_tahun` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelompok`
--

INSERT INTO `kelompok` (`id_kelompok`, `id_siswa`, `id_kelas`, `id_tahun`) VALUES
(1, 1, 55, 1),
(2, 4, 56, 1),
(3, 6, 56, 1),
(4, 7, 56, 1),
(5, 8, 56, 1),
(6, 9, 56, 1),
(7, 10, 56, 1),
(8, 11, 56, 1),
(9, 12, 56, 1),
(10, 13, 56, 1),
(11, 14, 56, 1),
(12, 15, 56, 1),
(13, 16, 56, 1),
(14, 17, 56, 1),
(15, 18, 56, 1),
(16, 19, 56, 1),
(17, 20, 56, 1),
(18, 21, 56, 1),
(19, 22, 56, 1),
(20, 23, 56, 1),
(21, 24, 56, 1),
(22, 25, 56, 1),
(23, 26, 56, 1),
(24, 27, 56, 1),
(25, 28, 56, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `keterampilan`
--

CREATE TABLE `keterampilan` (
  `id_data_keterampilan` int(10) NOT NULL,
  `nama_kelas` varchar(100) NOT NULL,
  `id_mapel` varchar(100) NOT NULL,
  `deskripsi41` varchar(100) NOT NULL,
  `deskripsi42` varchar(100) NOT NULL,
  `deskripsi43` varchar(100) NOT NULL,
  `deskripsi44` varchar(100) NOT NULL,
  `deskripsi45` varchar(100) NOT NULL,
  `deskripsi46` varchar(100) NOT NULL,
  `deskripsi47` varchar(100) NOT NULL,
  `deskripsi48` varchar(100) NOT NULL,
  `deskripsi49` varchar(100) NOT NULL,
  `deskripsi410` varchar(100) NOT NULL,
  `deskripsi411` varchar(100) NOT NULL,
  `deskripsi412` varchar(100) NOT NULL,
  `deskripsi413` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `keterampilan`
--

INSERT INTO `keterampilan` (`id_data_keterampilan`, `nama_kelas`, `id_mapel`, `deskripsi41`, `deskripsi42`, `deskripsi43`, `deskripsi44`, `deskripsi45`, `deskripsi46`, `deskripsi47`, `deskripsi48`, `deskripsi49`, `deskripsi410`, `deskripsi411`, `deskripsi412`, `deskripsi413`, `username`) VALUES
(16, 'X-1', '7001', 'jgjhgsdsdfdsf', 'jhgdsfdsf', 'ggsadsad', 'jhsadsad', 'gjh', 'gjh', 'gjh', 'gjh', 'g', 'jhg', 'jhgsadsad', 'jhgsdsadsad', 'jhsadsad', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ketidakhadiran`
--

CREATE TABLE `ketidakhadiran` (
  `id_sosial` int(10) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `ijin` varchar(10) NOT NULL,
  `alpha` varchar(10) NOT NULL,
  `sakit` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `leger`
--

CREATE TABLE `leger` (
  `id_leger` int(10) NOT NULL,
  `tahun_pelajaran` varchar(100) NOT NULL,
  `nama_kelas` varchar(200) NOT NULL,
  `semester` varchar(200) NOT NULL,
  `username` varchar(100) NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `leger`
--

INSERT INTO `leger` (`id_leger`, `tahun_pelajaran`, `nama_kelas`, `semester`, `username`) VALUES
(34564, '2018/2019', 'X-1', 'Genap', 'admin'),
(34565, 'sdsad', 'XII-a', 'Ganjil', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mapel`
--

CREATE TABLE `mapel` (
  `id_mapel` int(10) NOT NULL,
  `nama_mapel` varchar(40) NOT NULL,
  `kkm` varchar(10) NOT NULL,
  `nama_kelas` varchar(100) NOT NULL,
  `username` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `nama_mapel`, `kkm`, `nama_kelas`, `username`) VALUES
(7001, 'Al Quran Hadist', '75', 'X-1', 'admin'),
(7002, 'Aqidah Akhlak', '75', 'X-1', 'admin'),
(7003, 'Fiqih', '75', 'X-1', 'admin'),
(7004, 'Sejarah Kebudayaan Islam', '75', 'X-1', 'admin'),
(7005, 'Pendidikan Pancasila dan Kewarganegaraan', '75', 'X-1', 'admin'),
(7006, 'Bahasa Indonesia', '75', 'X-1', 'admin'),
(7007, 'Bahasa Arab', '75', 'XII-a', 'admin'),
(7008, 'Matematika', '70', 'X-1', 'admin'),
(7009, 'Ilmu Pengetahuan Alam', '70', 'X-1', 'admin'),
(7010, 'Ilmu Pengetahuan Sosial', '70', 'X-1', 'admin'),
(7011, 'Bahasa Inggris', '70', 'X-1', 'admin'),
(7012, 'Seni Budaya', '75', 'X-1', 'admin'),
(7013, 'Pendidikan Jasmani Olahraga dan Kesehata', '75', 'XII-a', 'admin'),
(7014, 'Prakarya', '75', 'XII-a', 'admin'),
(7015, 'Bahasa Daerah', '75', 'XII-a', 'admin'),
(7016, 'Aswaja', '75', 'XII-a', 'admin'),
(7017, 'Fathul Qarib', '65', 'XII-a', 'admin'),
(8001, 'Al Quran Hadist', '75', 'X-1', 'admin'),
(8002, 'Aqidah Akhlak', '75', 'X-1', 'admin'),
(8003, 'Fiqih', '75', 'XII-a', 'admin'),
(8004, 'Sejarah Kebudayaan Islam', '75', 'X-1', 'admin'),
(8005, 'Pendidikan Pancasila dan Kewarganegaraan', '75', 'XII-a', 'admin'),
(8006, 'Bahasa Indonesia', '75', 'X-1', 'admin'),
(8007, 'Bahasa Arab', '75', 'X-1', 'admin'),
(8008, 'Matematika', '70', 'X-1', 'admin'),
(8009, 'Ilmu Pengetahuan Alam', '70', 'X-1', ''),
(8010, 'Ilmu Pengetahuan Sosial', '70', 'XII-a', ''),
(8011, 'Bahasa Inggris', '70', 'XII-a', ''),
(8012, 'Seni BUdaya', '75', 'XII-a', ''),
(8013, 'Pendidikan Jasmani Olahraga dan Kesehata', '75', 'X-1', ''),
(8014, 'Prakarya', '75', 'X-1', ''),
(8015, 'Bahasa Daerah', '75', 'X-1', ''),
(8016, 'Aswaja', '75', 'X-1', ''),
(8017, 'Fathul Qarib', '65', 'XII-a', ''),
(9001, 'Al Quran Hadist', '75', 'X-1', ''),
(9002, 'Aqidah Akhlak', '75', 'X-1', ''),
(9003, 'Fiqih', '75', 'XII-a', ''),
(9004, 'Sejarah Kebudayaan Islam', '75', 'X-1', ''),
(9005, 'Pendidikan Pancasila dan Kewarganegaraan', '75', 'X-1', ''),
(9006, 'Bahasa Indonesia', '75', 'XII-a', ''),
(9007, 'Bahasa Arab', '75', 'X-1', ''),
(9008, 'Matematika', '70', 'X-1', ''),
(9009, 'Ilmu Pengetahuan Alam', '70', 'XII-a', ''),
(9010, 'Ilmu Pengetahuan Sosial', '70', 'X-1', ''),
(9011, 'Bahasa Inggris', '70', 'X-1', ''),
(9012, 'Seni BUdaya', '75', 'X-1', ''),
(9013, 'Pendidikan Jasmani Olahraga dan Kesehata', '75', 'XII-a', ''),
(9014, 'Prakarya', '75', 'X-1', ''),
(9015, 'Bahasa Daerah', '75', 'XII-a', ''),
(9016, 'Aswaja', '75', 'XII-a', ''),
(9017, 'Fathul Qarib', '65', 'X-1', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `member`
--

CREATE TABLE `member` (
  `id_member` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(5) NOT NULL,
  `id_parent` int(5) NOT NULL DEFAULT '0',
  `nama_menu` varchar(30) NOT NULL,
  `link` varchar(100) NOT NULL,
  `aktif` enum('Ya','Tidak') NOT NULL DEFAULT 'Ya'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`id_menu`, `id_parent`, `nama_menu`, `link`, `aktif`) VALUES
(9, 8, 'Hukum', 'kategori-48-hukum.html', 'Ya'),
(8, 0, 'Nasional', '#', 'Ya'),
(7, 0, 'Home', 'index.php', 'Ya'),
(11, 8, 'Politik', 'kategori-22-politik.html', 'Ya'),
(12, 8, 'Ekonomi', 'kategori-21-ekonomi.html', 'Ya'),
(13, 0, 'Internasional', 'kategori-39-internasional.html', 'Ya'),
(14, 0, 'Teknologi', 'kategori-19-teknologi.html', 'Ya'),
(18, 0, 'Olahraga', 'kategori-2-olahraga.html', 'Ya'),
(19, 0, 'Hiburan', 'kategori-23-hiburan.html', 'Ya'),
(20, 0, 'Metropolitan', 'kategori-41-metropolitan.html', 'Ya'),
(21, 0, 'Dunia Islam', 'kategori-42-dunia-islam.html', 'Ya'),
(22, 39, 'Kuliner', 'kategori-40-kuliner.html', 'Ya'),
(23, 0, 'Video', 'semua-playlist.html', 'Ya'),
(40, 39, 'Kesehatan', 'kategori-31-kesehatan.html', 'Ya'),
(39, 0, '+ Lainnya', '', 'Ya');

-- --------------------------------------------------------

--
-- Struktur dari tabel `modul`
--

CREATE TABLE `modul` (
  `id_modul` int(5) NOT NULL,
  `nama_modul` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `link` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `static_content` text COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `publish` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  `status` enum('user','admin') COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  `urutan` int(5) NOT NULL,
  `link_seo` varchar(50) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `modul`
--

INSERT INTO `modul` (`id_modul`, `nama_modul`, `username`, `link`, `static_content`, `gambar`, `publish`, `status`, `aktif`, `urutan`, `link_seo`) VALUES
(2, 'Manajemen User', '', '?module=user', '', '', 'Y', 'user', 'Y', 22, ''),
(18, 'Data Siswa', '', '?module=siswa', '', '', 'Y', 'user', 'Y', 5, 'semua-berita.html'),
(19, 'Spiritual', '', '?module=spiritual', '', '', 'N', 'user', 'N', 9, ''),
(10, 'Manajemen Modul', '', '?module=modul', '', '', 'Y', 'user', 'Y', 23, ''),
(31, 'Data Guru', '', '?module=guru', '', '', 'Y', 'user', 'Y', 6, ''),
(34, 'Kelas', '', '?module=kelas', '', '', 'Y', 'user', 'Y', 7, ''),
(35, 'Wali Kelas', '', '?module=wali', '', '', 'Y', 'user', 'Y', 8, ''),
(43, 'Ekstrakurikuler', '', '?module=ekstrakurikuler', '', '', 'Y', 'user', 'Y', 11, ''),
(44, 'Ketidakhadiran', '', '?module=ketidakhadiran', '', '', 'Y', 'user', 'Y', 12, ''),
(46, 'Sosial', '', '?module=sosial', '', '', 'Y', 'user', 'Y', 10, ''),
(61, 'Identitas Sekolah', '', '?module=identitas', '', '', 'Y', 'user', 'Y', 1, ''),
(62, 'Catatan Wali Kelas', '', '?module=catatan', '', '', 'Y', 'user', 'Y', 13, ''),
(71, 'Nilai Spiritual', 'admin', '?module=nilaispiritual', '', '', 'Y', 'user', 'Y', 24, ''),
(72, 'Nilai Sosial', 'admin', '?module=nilaisosial', '', '', 'Y', 'user', 'Y', 25, ''),
(73, 'Nilai Pengetahuan', 'admin', '?module=nilaipengetahuan', '', '', 'Y', 'user', 'Y', 26, ''),
(74, 'Nilai Ekstrakurikuler', 'admin', '?module=nilaiekstrakurikuler', '', '', 'Y', 'user', 'Y', 27, ''),
(75, 'Nilai Keterampilam', 'admin', '?module=nilaiketerampilan', '', '', 'Y', 'user', 'Y', 28, ''),
(76, 'Nilai Ketidakhadiran', 'admin', '?module=nilaiketidakhadiran', '', '', 'Y', 'user', 'Y', 29, ''),
(77, 'Nilai Harian', 'admin', '?module=nilaiharian', '', '', 'Y', 'user', 'Y', 30, ''),
(79, 'Nilai Akhir', 'admin', '?module=nilaiakhir', '', '', 'Y', 'user', 'Y', 31, ''),
(80, 'Mutasi Masuk', 'admin', '?module=mutasimask', '', '', 'Y', 'user', 'Y', 32, ''),
(81, 'Mutasi Keluar', 'admin', '?module=mutasikeluar', '', '', 'Y', 'user', 'Y', 33, ''),
(82, 'Prestasi', 'admin', '?module=prestasi', '', '', 'Y', 'user', 'Y', 34, ''),
(83, 'Grafik Siswa', 'admin', '?module=grafik', '', '', 'Y', 'user', 'Y', 35, ''),
(84, 'Ubah Password', 'admin', '?module=ubahpassword', '', '', 'Y', 'user', 'Y', 36, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mutasikeluar`
--

CREATE TABLE `mutasikeluar` (
  `id_mutasikeluar` int(10) NOT NULL,
  `nisn` varchar(100) NOT NULL,
  `nama_kelas` varchar(100) NOT NULL,
  `sebab_keluar` varchar(100) NOT NULL,
  `nama_siswa` varchar(200) NOT NULL,
  `tanggal_keluar` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mutasimasuk`
--

CREATE TABLE `mutasimasuk` (
  `id_mutasimasuk` int(10) NOT NULL,
  `nisn` varchar(100) NOT NULL,
  `tahun_pelajaran` varchar(100) NOT NULL,
  `nama_kelas` varchar(100) NOT NULL,
  `sekolah_asal` varchar(100) NOT NULL,
  `nama_siswa` varchar(200) NOT NULL,
  `tanggal_masuk` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilaiakhir`
--

CREATE TABLE `nilaiakhir` (
  `id_nilaiakhir` int(10) NOT NULL,
  `tahun_pelajaran` varchar(100) NOT NULL,
  `nama_kelas` varchar(100) NOT NULL,
  `semester` varchar(100) NOT NULL,
  `nama_siswa` varchar(200) NOT NULL,
  `ph` varchar(200) NOT NULL,
  `pts` varchar(100) NOT NULL,
  `pas` varchar(100) NOT NULL,
  `nr` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilaiekskul`
--

CREATE TABLE `nilaiekskul` (
  `id_nilaiekskul` int(11) NOT NULL,
  `id_tahun` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_siswa` int(11) DEFAULT NULL,
  `id_ekskul` int(11) DEFAULT NULL,
  `keterangan` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilaiharian`
--

CREATE TABLE `nilaiharian` (
  `id_nilaiharian` int(11) NOT NULL,
  `id_tahun` int(11) DEFAULT NULL,
  `id_mapel` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_siswa` int(11) DEFAULT NULL,
  `n1` int(11) DEFAULT NULL,
  `n2` int(11) DEFAULT NULL,
  `n3` int(11) DEFAULT NULL,
  `n4` int(11) DEFAULT NULL,
  `n5` int(11) DEFAULT NULL,
  `n6` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilaiketerampilan`
--

CREATE TABLE `nilaiketerampilan` (
  `id_nilaiketerampilan` int(11) NOT NULL,
  `id_tahun` int(11) DEFAULT NULL,
  `id_mapel` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `jenis_ujian` varchar(100) DEFAULT NULL,
  `id_siswa` int(11) DEFAULT NULL,
  `n1` int(11) DEFAULT NULL,
  `n2` int(11) DEFAULT NULL,
  `n3` int(11) DEFAULT NULL,
  `n4` int(11) DEFAULT NULL,
  `n5` int(11) DEFAULT NULL,
  `n6` int(11) DEFAULT NULL,
  `n7` int(11) DEFAULT NULL,
  `n8` int(11) DEFAULT NULL,
  `n9` int(11) DEFAULT NULL,
  `n10` int(11) DEFAULT NULL,
  `n11` int(11) DEFAULT NULL,
  `n12` int(11) DEFAULT NULL,
  `n13` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilaiketidakhadiran`
--

CREATE TABLE `nilaiketidakhadiran` (
  `id_nilaiketidakhadiran` int(11) NOT NULL,
  `id_tahun` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_siswa` int(11) DEFAULT NULL,
  `n1` int(11) DEFAULT NULL,
  `n2` int(11) DEFAULT NULL,
  `n3` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilaipengetahuan`
--

CREATE TABLE `nilaipengetahuan` (
  `id_nilaipengetahuan` int(10) NOT NULL,
  `id_tahun` int(11) DEFAULT NULL,
  `id_mapel` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `jenis_ujian` varchar(100) DEFAULT NULL,
  `id_siswa` int(11) DEFAULT NULL,
  `n1` int(11) DEFAULT NULL,
  `n2` int(11) DEFAULT NULL,
  `n3` int(11) DEFAULT NULL,
  `n4` int(11) DEFAULT NULL,
  `n5` int(11) DEFAULT NULL,
  `n6` int(11) DEFAULT NULL,
  `n7` int(11) DEFAULT NULL,
  `n8` int(11) DEFAULT NULL,
  `n9` int(11) DEFAULT NULL,
  `n10` int(11) DEFAULT NULL,
  `n11` int(11) DEFAULT NULL,
  `n12` int(11) DEFAULT NULL,
  `n13` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilaisosial`
--

CREATE TABLE `nilaisosial` (
  `id_nilaisosial` int(11) NOT NULL,
  `id_tahun` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_siswa` int(11) DEFAULT NULL,
  `n1` varchar(10) DEFAULT NULL,
  `n2` varchar(10) DEFAULT NULL,
  `n3` varchar(10) DEFAULT NULL,
  `n4` varchar(10) DEFAULT NULL,
  `n5` varchar(10) DEFAULT NULL,
  `n6` varchar(10) DEFAULT NULL,
  `n7` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilaispiritual`
--

CREATE TABLE `nilaispiritual` (
  `id_nilaispiritual` int(11) NOT NULL,
  `id_tahun` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_siswa` int(11) DEFAULT NULL,
  `n1` varchar(10) DEFAULT NULL,
  `n2` varchar(10) DEFAULT NULL,
  `n3` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengampu`
--

CREATE TABLE `pengampu` (
  `id_pengampu` int(10) NOT NULL,
  `nama_kelas` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `nama_guru` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `nama_mapel` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `nip` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `username` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengampu`
--

INSERT INTO `pengampu` (`id_pengampu`, `nama_kelas`, `nama_guru`, `nama_mapel`, `nip`, `username`) VALUES
(1, 'IX', '', '7008', '22222', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengetahuan`
--

CREATE TABLE `pengetahuan` (
  `id_data_pengetahuan` int(10) NOT NULL,
  `nama_kelas` varchar(100) NOT NULL,
  `id_mapel` varchar(100) NOT NULL,
  `deskripsi31` varchar(100) NOT NULL,
  `deskripsi32` varchar(100) NOT NULL,
  `deskripsi33` varchar(100) NOT NULL,
  `deskripsi34` varchar(100) NOT NULL,
  `deskripsi35` varchar(100) NOT NULL,
  `deskripsi36` varchar(100) NOT NULL,
  `deskripsi37` varchar(100) NOT NULL,
  `deskripsi38` varchar(100) NOT NULL,
  `deskripsi39` varchar(100) NOT NULL,
  `deskripsi310` varchar(100) NOT NULL,
  `deskripsi311` varchar(100) NOT NULL,
  `deskripsi312` varchar(100) NOT NULL,
  `deskripsi313` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesan`
--

CREATE TABLE `pesan` (
  `id_pesan` int(11) NOT NULL,
  `id_users` int(11) DEFAULT NULL,
  `pesan` mediumtext,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Struktur dari tabel `prestasi`
--

CREATE TABLE `prestasi` (
  `id_prestasi` int(10) NOT NULL,
  `nisn` varchar(100) NOT NULL,
  `nama_kelas` varchar(100) NOT NULL,
  `kurikuler` varchar(100) NOT NULL,
  `nama_siswa` varchar(200) NOT NULL,
  `ekstrakurikuler` varchar(200) NOT NULL,
  `catatan` varchar(200) NOT NULL,
  `username` varchar(100) NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `prestasi`
--

INSERT INTO `prestasi` (`id_prestasi`, `nisn`, `nama_kelas`, `kurikuler`, `nama_siswa`, `ekstrakurikuler`, `catatan`, `username`) VALUES
(34563, '1034445003', '', 'dfdsf', 'Angga', 'OSIS', 'Tingkatkan', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rapor`
--

CREATE TABLE `rapor` (
  `id_rapor` int(10) NOT NULL,
  `tahun_pelajaran` varchar(100) NOT NULL,
  `nama_kelas` varchar(200) NOT NULL,
  `semester` varchar(200) NOT NULL,
  `nisn` varchar(100) NOT NULL,
  `nama_siswa` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rapor`
--

INSERT INTO `rapor` (`id_rapor`, `tahun_pelajaran`, `nama_kelas`, `semester`, `nisn`, `nama_siswa`, `username`) VALUES
(34567, '2018/2019', 'X-1', 'Ganjil', '1234500004', 'Fernanda', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `id` int(11) NOT NULL,
  `id_users` int(11) DEFAULT NULL,
  `nis` varchar(100) DEFAULT NULL,
  `nisn` varchar(20) DEFAULT NULL,
  `nama_siswa` varchar(40) DEFAULT NULL,
  `namaPanggilan` varchar(20) DEFAULT NULL,
  `ttlSiswa` varchar(30) DEFAULT NULL,
  `jenisKelamin` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `anakKe` varchar(5) DEFAULT NULL,
  `alamatSiswa` varchar(40) DEFAULT NULL,
  `noTelpon` varchar(20) DEFAULT NULL,
  `sekolahAsal` varchar(40) DEFAULT NULL,
  `diterimaDikelas` varchar(10) DEFAULT NULL,
  `tglDiterima` varchar(30) DEFAULT NULL,
  `sttb` varchar(20) DEFAULT NULL,
  `namaAyah` varchar(40) DEFAULT NULL,
  `namaIbu` varchar(40) DEFAULT NULL,
  `alamatOrtu` varchar(40) DEFAULT NULL,
  `tlpOrtu` varchar(20) DEFAULT NULL,
  `kerjaAyah` varchar(20) DEFAULT NULL,
  `kerjaIbu` varchar(20) DEFAULT NULL,
  `namaWali` varchar(40) DEFAULT NULL,
  `alamatWali` varchar(40) DEFAULT NULL,
  `tlpWali` varchar(20) DEFAULT NULL,
  `kerjaWali` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`id`, `id_users`, `nis`, `nisn`, `nama_siswa`, `namaPanggilan`, `ttlSiswa`, `jenisKelamin`, `agama`, `status`, `anakKe`, `alamatSiswa`, `noTelpon`, `sekolahAsal`, `diterimaDikelas`, `tglDiterima`, `sttb`, `namaAyah`, `namaIbu`, `alamatOrtu`, `tlpOrtu`, `kerjaAyah`, `kerjaIbu`, `namaWali`, `alamatWali`, `tlpWali`, `kerjaWali`) VALUES
(1, 3, '1010101', '1034445002', 'Habibah', 'B', 'Lamongan, 08 Agustus 2003', 'Perempuan', 'Islam', 'Anak Kandung', '2', 'Lamongan', '081655724444', 'SD Islam Surabaya', 'VII', '10 Juli 2015', '1232', 'Abdullah', 'Ruqoiyah', 'Lamongan', '085733442222', 'Guru', 'Guru', 'Abdullah', 'Lamongan', '085733445777', 'Guru'),
(2, 5, '1010102', '1034445001', 'Bayu', 'B', 'Lamongan, 08 Agustus 2003', 'Laki-Laki', 'Islam', 'Anak Kandung', '2', 'Lamongan', '081655724444', 'SD Islam Surabaya', 'VII', '10 Juli 2015', '1232', 'Abdullah', 'Ruqoiyah', 'Lamongan', '085733442222', 'Guru', 'Guru', 'Abdullah', 'Lamongan', '085733445777', 'Guru'),
(4, 27, '714\r\n', '0047109514', 'AH. SYAIFUDIN', 'UDIN', NULL, 'L', 'Islam', 'Anak Kandung', NULL, NULL, NULL, 'MI. MA\'ARIF AT-TAQWA', 'VII - 1', '27 JULI 2015', NULL, 'SUKURDI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 28, '715\r\n', '0043805422', 'AHMAD ZAINI', 'ZAINI', NULL, 'L', 'Islam', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 29, '716\r\n', '0046587184', 'BAHRUL ILMI', 'BAHRUL', NULL, 'L', 'Islam', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 30, '717', '0042238474', 'BAYU GILANG LADIKA', 'BAYU', NULL, 'L', 'Islam', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 31, '718', '0042238479', 'EKA MANZYLIR ROHMAH', 'EKA', NULL, 'P', 'ISLAM', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 32, '719\r\n', '0039329811', 'FITRI WULANDARI', 'FITRI', NULL, 'P', 'Islam', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 33, '720\r\n', '0042238483', 'INAYATUL KHARITSA', 'INA', NULL, 'P', 'ISLAM', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 34, '721\r\n', '0038908230', 'IRMA JAYANTI', 'IRMA', NULL, 'P', 'Islam', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 48, '722\r\n', '0036151103', 'LU\'LU\' NAILUL BAROKAH', 'LU\'LU', NULL, 'P', 'ISLAM', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 35, '723\r\n', '0036151098', 'LUTFATIN NIHAYAH', 'LUTFA', NULL, 'P', 'Islam', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 36, '724\r\n', '0042238477', 'M. AFRIZAL SYAPUTRA', 'AFRIZAL', NULL, 'L', 'ISLAM', 'ANAK KANDUNG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 37, '725\r\n', NULL, 'M. RIZAL AKBAR', 'RIZAL', NULL, 'L', 'Islam', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 38, '726\r\n', '0042238480', 'M.WAWAN EFENDI', 'WAWAN', NULL, 'L', 'ISLAM', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 39, '727\r\n', '0036151099', 'MOH. ARFI KURNIA ROHMAN', 'ARFI', NULL, 'L', 'Islam', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 40, '728\r\n', '0042238472', 'MOH. RIDWAN HISYAMUDIN', 'RIDWAN', NULL, 'L', 'ISLAM', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 41, '729\r\n', '0042238490', 'MOH. YAZID HIDAYAT', 'YAZID', NULL, 'L', 'Islam', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 42, '730\r\n', NULL, 'MOHAMAD SHODIQ', 'SHODIQ', NULL, 'L', 'ISLAM', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 46, '733\r\n', '0042238481', 'PUTRI NAILUS SA\'ADAH', 'PUTRI', NULL, 'P', 'Islam', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 45, '734\r\n', '0042238476', 'RIEKE DIAH ANGGRAINI', 'RIEKE', NULL, 'P', 'ISLAM', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 47, '735\r\n', '0042238493', 'RINALDI DEVA PRANATA', 'RINALDI', NULL, 'L', 'Islam', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 49, '736\r\n', '0042238473', 'SIGIT MARDIANTO', 'SIGIT', NULL, 'L', 'ISLAM', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 43, '731\r\n', '0042238470', 'MUHAMAD TAUFIK EFENDI', 'TAUFIK', NULL, 'L', 'Islam', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 44, '732\r\n', '0042238484', 'NADYA ROSYADA', 'NADYA', NULL, 'P', 'ISLAM', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 52, '737\r\n', '0003615110', 'DEBBY SHINTIYA DEWI', 'DEBBY', NULL, 'P', 'Islam', 'Anak Kandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sosial`
--

CREATE TABLE `sosial` (
  `idSikapSosial` int(10) NOT NULL,
  `predikat` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sosial`
--

INSERT INTO `sosial` (`idSikapSosial`, `predikat`, `deskripsi`, `kategori`, `username`) VALUES
(13, 'SB', 'mempunyai nilai jujur yang sangat baik', 'Jujur', 'admin'),
(14, 'B', 'mempunyai nilai kejujuran yang baik', 'Jujur', 'admin'),
(15, 'SM', 'sudah mampu dalam jujur', 'Jujur', 'admin'),
(16, 'PB', 'dengan bimbingan dan pendampingan yang lebih akan meningkatkan sifat jujur ', 'Jujur', 'admin'),
(17, 'SB', 'mempunyai kedisipilan yang sangat baik', 'Disiplin', 'admin'),
(18, 'B', 'mempunyai kedisiplinan yang baik', 'Disiplin', 'admin'),
(19, 'SM', 'sudah mampu dalam disiplin', 'Disiplin', 'admin'),
(20, 'PB', 'dengan bimbingan dan pendampingan yang lebih akan meningkatkan kedisiplinan pada Ananda', 'Disiplin', 'admin'),
(21, 'SB', 'mempunyai sikap tanggung jawab yang sangat baik', 'Tanggung Jawab', 'admin'),
(22, 'B', 'mempunyai sikap tanggung jawab yang baik', 'Tanggung Jawab', 'admin'),
(23, 'SM', 'sudah mampu dalam bertanggung jawab', 'Tanggung Jawab', 'admin'),
(24, 'PB', 'dengan bimbingan dan pendampingan yang lebih akan meningkatkan sikap tanggung jawab ', 'Tanggung Jawab', 'admin'),
(25, 'SB', 'mempunyai sikap toleransi yang sangat baik', 'Toleransi ', 'admin'),
(26, 'B', 'mempunyai sikap toleransi yang baik', 'Toleransi ', 'admin'),
(27, 'SM', 'sudah mampu dalam bertoleransi', 'Toleransi ', 'admin'),
(28, 'PB', 'dengan bimbingan dan pendampingan yang lebih akan meningkatkan sikap bertoleransi', 'Toleransi ', 'admin'),
(29, 'SB', 'mempunyai sikap gotong royong yang sangat baik', 'Gotong Royong ', 'admin'),
(30, 'B', 'mempunyai sikap gotong royong yang baik', 'Gotong Royong ', 'admin'),
(31, 'SM', 'sudah mampu dalam bergotong royong', 'Gotong Royong ', 'admin'),
(32, 'PB', 'dengan bimbingan dan pendampingan yang lebih akan  meningkatkan sikap bergotong royong', 'Gotong Royong ', 'admin'),
(33, 'SB', 'mempunyai sikap santun yang sangat baik', 'Santun', 'admin'),
(34, 'B', 'mempunyai sikap santun yang baik', 'Santun', 'admin'),
(35, 'SM', 'sudah mampu dalam bersantun', 'Santun', 'admin'),
(36, 'PB', 'dengan bimbingan dan pendampingan yang lebih akan  meningkatkan sikap santun', 'Santun', 'admin'),
(40, 'SB', 'mempunyai sikap percaya diri yang sangat baik', 'Percaya diri ', 'admin'),
(41, 'B', 'mempunyai sikap percaya diri yang baik', 'Percaya diri ', 'admin'),
(42, 'SM', 'sudah mampu menanamkan sikap percaya diri', 'Percaya diri ', 'admin'),
(43, 'PB', 'dengan bimbingan dan pendampingan yang lebih akan  meningkatkan sikap percaya diri', 'Percaya diri ', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spiritual`
--

CREATE TABLE `spiritual` (
  `idSikapSpiritual` int(10) NOT NULL,
  `predikat` varchar(2) NOT NULL,
  `deskripsi` text NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spiritual`
--

INSERT INTO `spiritual` (`idSikapSpiritual`, `predikat`, `deskripsi`, `kategori`, `username`) VALUES
(1, 'SB', 'Sangat baik dalam berdoa sebelum dan sesudah melakukan kegiatan', 'Berdoa', 'admin'),
(2, 'B', 'baik dalam berdoa sebelum dan sesudah melakukan kegiatan', 'Berdoa', 'admin'),
(3, 'SM', 'sudah mampu dalam berdoa sebelum dan sesudah melakukan kegiatan', 'Berdoa', 'admin'),
(4, 'PB', 'dengan bimbingan dan pendampingan yang lebih akan meningkatkan sikap berdoa sebelum dan sesudah melakukan kegiatan', 'Baca Al Quran', 'admin'),
(5, 'SB', 'Sangat baik dalam membaca Al-Quran', 'Berdoa', 'admin'),
(6, 'B', 'baik dalam membaca Al-Quran', 'Baca Al Quran', 'admin'),
(7, 'SM', 'sudah mampu dalam membaca Al-Quran', 'Berdoa', 'admin'),
(8, 'PB', 'dengan bimbingan dan pendampingan yang lebih akan meningkatkan Ananda dalam membaca Al-Quran', 'Berdoa', 'admin'),
(9, 'SB', 'sangat baik dalam kebiasaan mengucapkan salam', 'Baca Al Quran', 'admin'),
(10, 'B', 'baik dalam kebiasaan  mengucapkan salam', 'Salam', 'admin'),
(11, 'SM', 'sudah mampu dalam terbiasa mengucapkan salam', 'Salam', 'admin'),
(12, 'PB', 'dengan bimbingan dan pendampingan yang lebih akan meningkatkan Ananda dalam kebiasaan mengucapkan salam', 'Salam', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahun`
--

CREATE TABLE `tahun` (
  `id_tahun` int(11) NOT NULL,
  `semester` varchar(100) DEFAULT NULL,
  `tahun` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tahun`
--

INSERT INTO `tahun` (`id_tahun`, `semester`, `tahun`) VALUES
(1, 'Ganjil', '2018/2019'),
(3, 'Genap', '2018/2019');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_users` int(11) NOT NULL,
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `no_telp` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `foto` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `level` varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT 'user',
  `blokir` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `id_session` varchar(100) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_users`, `username`, `password`, `nama_lengkap`, `email`, `no_telp`, `foto`, `level`, `blokir`, `id_session`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', 'admin@gmail.com', '', '9user.png', 'admin', 'N', 'q173s8hs1jl04st35169ccl8o7'),
(7, 'guru2', '77e69c137812518e359196bb2f5e9bb9', 'Moh. Alimun, SE., ME.', 'guru@gmail.com', '', '91user.png', 'guru', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(3, 'siswa1', 'bcd724d15cde8c47650fda962968f102', 'Habibah', 'Habibah@gmail.com', '', '17user-6.png', 'siswa', 'N', 'bcd724d15cde8c47650fda962968f102'),
(5, 'siswa2', 'bcd724d15cde8c47650fda962968f102', 'Bayu', 'Bayu@gmail.com', '', '17user-6.png', 'siswa', 'N', 'bcd724d15cde8c47650fda962968f102'),
(6, 'guru1', '77e69c137812518e359196bb2f5e9bb9', 'Agus Sugatot, M.Pd.', 'guru@gmail.com', '', '91user.png', 'wali', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(8, 'guru3', '77e69c137812518e359196bb2f5e9bb9', 'Drs. H. Moh Musa', 'guru@gmail.com', '', '91user.png', 'guru', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(9, 'guru4', '77e69c137812518e359196bb2f5e9bb9', 'Mustajab, S.Pd.', 'guru@gmail.com', '', '91user.png', 'guru', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(10, 'guru5', '77e69c137812518e359196bb2f5e9bb9', 'ASMAUR ROHMAN, S.Pd.', 'guru@gmail.com', '', '91user.png', 'wali', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(11, 'guru6', '77e69c137812518e359196bb2f5e9bb9', 'ABD. BASIR SADZALI, S.Pd.', 'guru@gmail.com', '', '91user.png', 'guru', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(12, 'guru7', '77e69c137812518e359196bb2f5e9bb9', 'CHOIRUL HADI, S.Ag.', 'guru@gmail.com', '', '91user.png', 'guru', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(13, 'guru8', '77e69c137812518e359196bb2f5e9bb9', 'ALI IRFANI', 'guru@gmail.com', '', '91user.png', 'wali', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(14, 'guru9', '77e69c137812518e359196bb2f5e9bb9', 'SHOHIHAH, S.Pd.', 'guru@gmail.com', '', '91user.png', 'guru', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(15, 'guru10', '77e69c137812518e359196bb2f5e9bb9', 'ULYATUL MABRUROH, S.Pd.', 'guru@gmail.com', '', '91user.png', 'guru', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(16, 'guru11', '77e69c137812518e359196bb2f5e9bb9', 'MAHSULI ARIF, S.Ag.', 'guru@gmail.com', '', '91user.png', 'guru', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(17, 'guru12', '77e69c137812518e359196bb2f5e9bb9', 'ABD. RASYID S.Pd.', 'guru@gmail.com', '', '91user.png', 'guru', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(18, 'guru13', '77e69c137812518e359196bb2f5e9bb9', 'Muzayyanah, S.Pd.', 'guru@gmail.com', '', '91user.png', 'guru', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(19, 'guru14', '77e69c137812518e359196bb2f5e9bb9', 'Ifatun Ni mah, S.Pd.', 'guru@gmail.com', '', '91user.png', 'guru', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(20, 'guru15', '77e69c137812518e359196bb2f5e9bb9', 'Sonhaji, S.Pd.', 'guru@gmail.com', '', '91user.png', 'guru', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(21, 'guru16', '77e69c137812518e359196bb2f5e9bb9', 'Junaidi, S.Pd.', 'guru@gmail.com', '', '91user.png', 'wali', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(22, 'guru17', '77e69c137812518e359196bb2f5e9bb9', 'Harmaji, S.Pd', 'guru@gmail.com', '', '91user.png', 'guru', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(23, 'guru18', '77e69c137812518e359196bb2f5e9bb9', 'Habibah', 'guru@gmail.com', '', '91user.png', 'wali', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(24, 'guru19', '77e69c137812518e359196bb2f5e9bb9', 'Moh. Syahidan, S.Pd.', 'guru@gmail.com', '', '91user.png', 'guru', 'N', '5bi6b98a7r02hvh15dsog2vfo2'),
(26, 'baru', '5ef035d11d74713fcb36f2df26aa7c3d', 'baru', 'baru@gmail.com', '087856850432', '91user.png', 'admin', 'N', '5ef035d11d74713fcb36f2df26aa7c3d'),
(27, 'siswa-714', 'bcd724d15cde8c47650fda962968f102', 'AH. SYAIFUDIN', 'ahsyaifudin@gmail.com', '', '3317user-6.png', 'siswa', 'N', ''),
(28, 'siswa-715', 'bcd724d15cde8c47650fda962968f102', 'AHMAD ZAINI', 'ahmadzaini@gmail.com', '', '3217user-6.png', 'siswa', 'N', ''),
(29, 'siswa-716', 'bcd724d15cde8c47650fda962968f102', 'BAHRUL ILMI', 'bahrulilmi@gmail.com', '', '5217user-6.png', 'siswa', 'N', ''),
(30, 'siswa-717', 'bcd724d15cde8c47650fda962968f102', 'BAYU GILANG LADIKA', 'bayugilangladika@gmail.com', '', '2217user-6.png', 'siswa', 'N', ''),
(31, 'siswa-718', 'bcd724d15cde8c47650fda962968f102', 'EKA MANZYLIR ROHMAH', 'ekamanzylirrohmah@gmail.com', '', '7817user-6.png', 'siswa', 'N', ''),
(32, 'siswa-719', 'bcd724d15cde8c47650fda962968f102', 'FITRI WULANDARI', 'fitriwulandari@gmail.com', '', '6517user-6.png', 'siswa', 'N', ''),
(33, 'siswa-720', 'bcd724d15cde8c47650fda962968f102', 'INAYATUL KHARITSA', 'inayatulkharitsa@gmail.com', '', '4717user-6.png', 'siswa', 'N', ''),
(34, 'siswa-721', 'bcd724d15cde8c47650fda962968f102', 'IRMA JAYANTI', 'irmajayanti@gmail.com', '', '8417user-6.png', 'siswa', 'N', ''),
(35, 'siswa-723', 'bcd724d15cde8c47650fda962968f102', 'LUTFATIN NIHAYAH', 'lutfatinnihayah@gmail.com', '', '5053user-6.png', 'siswa', 'N', ''),
(36, 'siswa-724', 'bcd724d15cde8c47650fda962968f102', 'M. AFRIZAL SYAPUTRA', 'mafrizalsyaputra@gmail.com', '', '2817user-6.png', 'siswa', 'N', ''),
(37, 'siswa-725', 'bcd724d15cde8c47650fda962968f102', 'M. RIZAL AKBAR', 'mrizalakbar@gmail.com', '', '3753user-6.png', 'siswa', 'N', ''),
(38, 'siswa-726', 'bcd724d15cde8c47650fda962968f102', 'M.WAWAN EFENDI', 'mwawanefendi@gmail.com', '', '9853user-6.png', 'siswa', 'N', ''),
(39, 'siswa-727', 'bcd724d15cde8c47650fda962968f102', 'MOH. ARFI KURNIA ROHMAN', 'moharfikurniarohman@gmail.com', '', '9133user-6.png', 'siswa', 'N', ''),
(40, 'siswa-728', 'bcd724d15cde8c47650fda962968f102', 'MOH. RIDWAN HISYAMUDIN', 'mohridwanhisyamudin@gmail.com', '', '3653user-6.png', 'siswa', 'N', ''),
(41, 'siswa-729', 'bcd724d15cde8c47650fda962968f102', 'MOH. YAZID HIDAYAT', 'mohyazidhidayat@gmail.com', '', '6753user-6.png', 'siswa', 'N', ''),
(42, 'siswa-730', 'bcd724d15cde8c47650fda962968f102', 'MOHAMAD SHODIQ', 'mohamadshodiq@gmail.com', '', '6753user-6.png', 'siswa', 'N', ''),
(43, 'siswa-731', 'bcd724d15cde8c47650fda962968f102', 'MUHAMAD TAUFIK EFENDI', 'muhamadtaufikefendi@gmail.com', '', '7653user-6.png', 'siswa', 'N', ''),
(44, 'siswa-732', 'bcd724d15cde8c47650fda962968f102', 'NADYA ROSYADA', 'nadyarosyada@gmail.com', '', '3333user-6.png', 'siswa', 'N', ''),
(45, 'siswa-734', 'bcd724d15cde8c47650fda962968f102', 'RIEKE DIAH ANGGRAINI', 'riekediahanggraini@gmail.com', '', '2953user-6.png', 'siswa', 'N', ''),
(46, 'siswa-733', 'bcd724d15cde8c47650fda962968f102', 'PUTRI NAILUS SA\'ADAH\r\n', 'putrinailussa\'adah@gmail.com\r\n', '', '2953user-6.png', 'siswa', 'N', ''),
(47, 'siswa-735', 'bcd724d15cde8c47650fda962968f102', 'RINALDI DEVA PRANATA\r\n', 'rinaldidevapranata@gmail.com\r\n', '', '2953user-6.png', 'siswa', 'N', ''),
(48, 'siswa-722', 'bcd724d15cde8c47650fda962968f102', 'LU\'LU\' NAILUL BAROKAH\r\n', 'lu\'lu\'nailulbarokah@gmail.com\r\n', '', '6753user-6.png', 'siswa', 'N', ''),
(49, 'siswa-736', 'bcd724d15cde8c47650fda962968f102', 'SIGIT MARDIANTO\r\n', 'sigitmardianto@gmail.com\r\n', '', '6753user-6.png', 'siswa', 'N', ''),
(52, 'siswa-737', 'bcd724d15cde8c47650fda962968f102', 'DEBBY SHINTIYA DEWI\r\n', 'debbyshintiyadewi@gmail.com\r\n', '', '6753user-6.png', 'siswa', 'N', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `walikelas`
--

CREATE TABLE `walikelas` (
  `id_wali` int(5) NOT NULL,
  `nip` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `nama_kelas` varchar(100) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `walikelas`
--

INSERT INTO `walikelas` (`id_wali`, `nip`, `nama_kelas`) VALUES
(3493, '22222', 'IX');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `catatan`
--
ALTER TABLE `catatan`
  ADD PRIMARY KEY (`id_catatan`);

--
-- Indeks untuk tabel `ekstrakurikuler`
--
ALTER TABLE `ekstrakurikuler`
  ADD PRIMARY KEY (`idEkskul`);

--
-- Indeks untuk tabel `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indeks untuk tabel `identitas`
--
ALTER TABLE `identitas`
  ADD PRIMARY KEY (`id_identitas`);

--
-- Indeks untuk tabel `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indeks untuk tabel `kelompok`
--
ALTER TABLE `kelompok`
  ADD PRIMARY KEY (`id_kelompok`);

--
-- Indeks untuk tabel `keterampilan`
--
ALTER TABLE `keterampilan`
  ADD PRIMARY KEY (`id_data_keterampilan`);

--
-- Indeks untuk tabel `ketidakhadiran`
--
ALTER TABLE `ketidakhadiran`
  ADD PRIMARY KEY (`id_sosial`);

--
-- Indeks untuk tabel `leger`
--
ALTER TABLE `leger`
  ADD PRIMARY KEY (`id_leger`);

--
-- Indeks untuk tabel `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indeks untuk tabel `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indeks untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indeks untuk tabel `modul`
--
ALTER TABLE `modul`
  ADD PRIMARY KEY (`id_modul`);

--
-- Indeks untuk tabel `mutasikeluar`
--
ALTER TABLE `mutasikeluar`
  ADD PRIMARY KEY (`id_mutasikeluar`);

--
-- Indeks untuk tabel `mutasimasuk`
--
ALTER TABLE `mutasimasuk`
  ADD PRIMARY KEY (`id_mutasimasuk`);

--
-- Indeks untuk tabel `nilaiakhir`
--
ALTER TABLE `nilaiakhir`
  ADD PRIMARY KEY (`id_nilaiakhir`);

--
-- Indeks untuk tabel `nilaiekskul`
--
ALTER TABLE `nilaiekskul`
  ADD PRIMARY KEY (`id_nilaiekskul`);

--
-- Indeks untuk tabel `nilaiharian`
--
ALTER TABLE `nilaiharian`
  ADD PRIMARY KEY (`id_nilaiharian`);

--
-- Indeks untuk tabel `nilaiketerampilan`
--
ALTER TABLE `nilaiketerampilan`
  ADD PRIMARY KEY (`id_nilaiketerampilan`);

--
-- Indeks untuk tabel `nilaiketidakhadiran`
--
ALTER TABLE `nilaiketidakhadiran`
  ADD PRIMARY KEY (`id_nilaiketidakhadiran`);

--
-- Indeks untuk tabel `nilaipengetahuan`
--
ALTER TABLE `nilaipengetahuan`
  ADD PRIMARY KEY (`id_nilaipengetahuan`);

--
-- Indeks untuk tabel `nilaisosial`
--
ALTER TABLE `nilaisosial`
  ADD PRIMARY KEY (`id_nilaisosial`);

--
-- Indeks untuk tabel `nilaispiritual`
--
ALTER TABLE `nilaispiritual`
  ADD PRIMARY KEY (`id_nilaispiritual`);

--
-- Indeks untuk tabel `pengampu`
--
ALTER TABLE `pengampu`
  ADD PRIMARY KEY (`id_pengampu`);

--
-- Indeks untuk tabel `pengetahuan`
--
ALTER TABLE `pengetahuan`
  ADD PRIMARY KEY (`id_data_pengetahuan`);

--
-- Indeks untuk tabel `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id_pesan`) USING BTREE;

--
-- Indeks untuk tabel `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id_prestasi`);

--
-- Indeks untuk tabel `rapor`
--
ALTER TABLE `rapor`
  ADD PRIMARY KEY (`id_rapor`);

--
-- Indeks untuk tabel `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `sosial`
--
ALTER TABLE `sosial`
  ADD PRIMARY KEY (`idSikapSosial`);

--
-- Indeks untuk tabel `spiritual`
--
ALTER TABLE `spiritual`
  ADD PRIMARY KEY (`idSikapSpiritual`);

--
-- Indeks untuk tabel `tahun`
--
ALTER TABLE `tahun`
  ADD PRIMARY KEY (`id_tahun`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_users`);

--
-- Indeks untuk tabel `walikelas`
--
ALTER TABLE `walikelas`
  ADD PRIMARY KEY (`id_wali`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `catatan`
--
ALTER TABLE `catatan`
  MODIFY `id_catatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `ekstrakurikuler`
--
ALTER TABLE `ekstrakurikuler`
  MODIFY `idEkskul` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `guru`
--
ALTER TABLE `guru`
  MODIFY `id_guru` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=704;

--
-- AUTO_INCREMENT untuk tabel `identitas`
--
ALTER TABLE `identitas`
  MODIFY `id_identitas` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT untuk tabel `kelompok`
--
ALTER TABLE `kelompok`
  MODIFY `id_kelompok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `keterampilan`
--
ALTER TABLE `keterampilan`
  MODIFY `id_data_keterampilan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `ketidakhadiran`
--
ALTER TABLE `ketidakhadiran`
  MODIFY `id_sosial` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `leger`
--
ALTER TABLE `leger`
  MODIFY `id_leger` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34566;

--
-- AUTO_INCREMENT untuk tabel `mapel`
--
ALTER TABLE `mapel`
  MODIFY `id_mapel` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9018;

--
-- AUTO_INCREMENT untuk tabel `member`
--
ALTER TABLE `member`
  MODIFY `id_member` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT untuk tabel `modul`
--
ALTER TABLE `modul`
  MODIFY `id_modul` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT untuk tabel `mutasikeluar`
--
ALTER TABLE `mutasikeluar`
  MODIFY `id_mutasikeluar` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `mutasimasuk`
--
ALTER TABLE `mutasimasuk`
  MODIFY `id_mutasimasuk` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `nilaiakhir`
--
ALTER TABLE `nilaiakhir`
  MODIFY `id_nilaiakhir` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `nilaiekskul`
--
ALTER TABLE `nilaiekskul`
  MODIFY `id_nilaiekskul` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `nilaiharian`
--
ALTER TABLE `nilaiharian`
  MODIFY `id_nilaiharian` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `nilaiketerampilan`
--
ALTER TABLE `nilaiketerampilan`
  MODIFY `id_nilaiketerampilan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `nilaiketidakhadiran`
--
ALTER TABLE `nilaiketidakhadiran`
  MODIFY `id_nilaiketidakhadiran` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `nilaipengetahuan`
--
ALTER TABLE `nilaipengetahuan`
  MODIFY `id_nilaipengetahuan` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `nilaisosial`
--
ALTER TABLE `nilaisosial`
  MODIFY `id_nilaisosial` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `nilaispiritual`
--
ALTER TABLE `nilaispiritual`
  MODIFY `id_nilaispiritual` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pengampu`
--
ALTER TABLE `pengampu`
  MODIFY `id_pengampu` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pengetahuan`
--
ALTER TABLE `pengetahuan`
  MODIFY `id_data_pengetahuan` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id_pesan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id_prestasi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34564;

--
-- AUTO_INCREMENT untuk tabel `rapor`
--
ALTER TABLE `rapor`
  MODIFY `id_rapor` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34568;

--
-- AUTO_INCREMENT untuk tabel `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `sosial`
--
ALTER TABLE `sosial`
  MODIFY `idSikapSosial` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT untuk tabel `spiritual`
--
ALTER TABLE `spiritual`
  MODIFY `idSikapSpiritual` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tahun`
--
ALTER TABLE `tahun`
  MODIFY `id_tahun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id_users` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT untuk tabel `walikelas`
--
ALTER TABLE `walikelas`
  MODIFY `id_wali` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3494;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
