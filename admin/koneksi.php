<?php
// http://phpbego.wordpress.com

define("DB_HOST", "127.0.0.1");
define("DB_NAME", "mts");
define("DB_USER", "root");
define("DB_PASS", "");

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

if ($mysqli->connect_errno) {
    echo "Failed to connect to Database: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

/* Please remove on production */
mysqli_report(MYSQLI_REPORT_ERROR);