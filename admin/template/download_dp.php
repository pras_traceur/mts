<?php

    $induk = $_GET['induk'];
    $nama = $_GET['nama'];
    $sekolah = "MTs AT-TAQWA KALANGANYAR";
    $kurikuler = $_GET['kurikuler'];
    $ekstrakurikuler = $_GET['ekstrakurikuler'];
    $catatan = $_GET['catatan'];

    include "docxtemplate.class.php";
    $doc = new docxtemplate('data_prestasi.docx');
    
    ob_clean();
    
    $doc->set('induk',$induk);
    $doc->set('nama',$nama);
    $doc->set('sekolah',$sekolah);
    $doc->set('kurikuler',$kurikuler);
    $doc->set('ekstrakurikuler',$ekstrakurikuler);
    $doc->set('catatan',$catatan);
    
    //$doc->saveAs('SURAT MUTASI MASUK.docx');
    $doc->downloadAs('Catatan Prestasi.docx');
    //header('Content-Type:application/msword');
    //header('Content-Disposition: attachment;filename=SURAT MUTASI MASUK.docx');
    //readfile('SURAT MUTASI MASUK.docx');
    //exit;

?>