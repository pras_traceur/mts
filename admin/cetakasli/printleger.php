<!DOCTYPE html>


<style type="text/css">
h2
{
position:relative;
}
h3
{
position:relative;
	font-family: Arial;
	font-size:11px;
}
th
{
padding:4px;

}
img
{
padding:4px;

}
table
{
border-collapse:collapse;
	font-family: Arial;
	font-size:12px;
}
table, th, td
{
border: 1px solid #cccccc;
}
th
{
height:50px;
}
td
{
height:10px;
vertical-align:bottom;
padding:4px;
}
img {
    max-width: 10%;
    height: auto;
    width: auto\9; /* ie8 */
}


/*
 tabel
*/
.list {
	border-collapse: collapse;
	width: 90%;
	border-top: 1px solid #BFBFBF;
	border-left: 1px solid #BFBFBF;
	margin-bottom: 20px; 	
	font-family: Tahoma; 
	font-size: 8pt;
	padding-bottom: 10px;
}
.list2 {
	border-collapse: collapse;
	width: 100%;
	margin-bottom: 20px; 	
	font-family: Tahoma; 
	font-size: 8pt;
	padding-bottom: 10px;
}
.list td {
	border-right: 1px solid #DDDDDD;
	border-bottom: 1px solid #DDDDDD;
	font-family:"Calibri";
	font-size:13px;
}

.list thead td {
	background-color: #E5E5E5; 
	padding: px 5px; text-transform: capitalize; 
}
.list thead td a, .list thead td {
	text-decoration: none; 
	color: #000000;
	font-weight: bold; 
	
}
.list tbody a {
	text-decoration: none; 
}
.list tbody td {
	vertical-align: middle;
	padding: 0px 2px;
}
.list tbody tr:odd {
	background: #FFFFFF; 
}
.list tbody tr:even {
	background: #E4EEF7;
}
.list .left {
	text-align: left;
	padding: 7px;
} 
.list .right {
	text-align: right;
	padding: 2px; 
}
.list .center {
	text-align: center;
	padding: 2px;
}
.list .asc {
	padding-right: 2px;
	background: url('images/asc.png') right center no-repeat;
}
.list .desc {
	padding-right: 9px;
	background: url('images/desc.png') right center no-repeat;
}
</style>


<html>
<head>
	<title>CETAK LAPORAN</title>
</head>
<body>

	<center><H1>MTs At Taqwa Kalinganyar</h1>
	<H2>Jl. Masjid AT-TAQWA Desa Kalanganganyar Kecamatan Karanggeneng</center></H2>
	

	<?php 
	include 'koneksi.php';
	?>

	<table border='1' style='width: 100%'>
		<tr>
			<th width='1%'>No</th>
			<th width='10%'>Tahun Pelajaran</th>
			<th>Nama Kelas</th>
			<th width='10%'>Semester</th>
		</tr>
		<?php 
		$no = 1;
		$sql = mysqli_query($koneksi,'select * from leger');
		while($data = mysqli_fetch_array($sql)){
		?>
		<tr>
			<td><center><?php echo $no++; ?></center></td>
			<td><center><?php echo $data['tahun_pelajaran']; ?></center></td>
			<td><?php echo $data['nama_kelas']; ?></td>
			<td><center><?php echo $data['semester']; ?></center></td>
			
		</tr>
		<?php 
		}
		?>
	</table>

	<script>
		window.print();
	</script>

</body>
</html>