<style>
.btn-group button {
  background-color: #4CAF50; 
  border: 1px solid green; 
  color: white; 
  padding: 10px 24px;
  cursor: pointer; 
  float: left; 
  height:100px;
}

.btn-group:after {
  content: "";
  clear: both;
  display: table;
}

.btn-group button:not(:last-child) {
  border-right: none; 
}

.btn-group button:hover {
  background-color: #3e8e41;
}
</style>

<!-- =============================================================================================================== -->
<center><h1>Nilai Ekstrakurikuler</h1></center>
<br>

<?php if(!$_GET['tahun']){ ?>
<div style="padding: 40px">
	<p>Tahun Semester:</p>

	<div class="btn-group" style="width:100%">
		<?php $tahun = mysql_query("SELECT * FROM tahun"); 
		$i = 1;
			while($t=mysql_fetch_array($tahun)){
				if($i == 5){
					echo '</div><br><div class="btn-group" style="width:100%">';
				}
		?>
	  		<button onclick="location.href='<?=$aksi?>?module=n_ekstrakurikuler&tahun=<?=$t[id_tahun]?>'" style="width:25%">Semester <?=$t[semester]?><h2 style="color: white;"><?=$t[tahun]?></h2></button>

	  	<?php $i++;} ?>
	</div>
</div>
<?php }else{ ?>

<?php if($_GET['kelas']){ ?>

<?php 
	$tahun = $_GET['tahun'];
	$kelas = $_GET['kelas'];
	$ujian = $_GET['ujian'];

    function get_ekstrakurikuler($tahun,$kelas,$ujian,$siswa,$param){
	  $hasil=mysql_query("SELECT * FROM nilaiekskul WHERE id_tahun='".$tahun."' and id_kelas='".$kelas."' and id_siswa='".$siswa."'");
      $h=mysql_fetch_array($hasil);
	    if($h){
    		return $h[$param];
    	}else{
    		return '';
    	}
    }
?>
<div style="padding: 40px">
  <a href="<?=$aksi?>?module=n_ekstrakurikuler&tahun=<?=$tahun?>" class="button"> Kembali</a>
</div>
<?php if($_GET['ujian']){?>
	
        <div class=container_12>   
        <center>Nilai Ekstrakurikuler</center>
        <div class=grid_12> 
              <div class=block-border> 
              <div class=block-header> 
              <h1>Nilai Ekstrakurikuler</h1>
              <span></span> 
              </div> 
               <div class='block-content'>
        		  
            <table id='table-example' class='table'>	    
            <thead>
            	<th>No</th>
            	<th>Nama</th>
        		<th>Ekstrakurikuler</th>
        		<th>Keterangan</th>  
            </thead>
            <tbody>
            <?php 
			$no = 1;
            
            $siswa = mysql_query("select s.id,s.nama_siswa from siswa s,kelompok k where k.id_siswa=s.id and k.id_kelas='".$kelas."' and k.id_tahun='".$tahun."'"); 

            	$no = 1;
				while($s=mysql_fetch_array($siswa)){ ?>
            	<tr class=gradeX>
	        		<td><?=$no?></td>
	        		<td><?=$s[nama_siswa]?></td>
	        		<td>

                <?php
                  $ekskul = mysql_query("SELECT * FROM ekstrakurikuler");
               ?>
                 <select class="select2" name="id_ekskul" onchange="update(<?=$s[id]?>,'id_ekskul',this,<?=$tahun?>,<?=$kelas?>,'<?=$ujian?>');">
                  <option value="">------------------------</option>
                  <?php 
                    while($r=mysql_fetch_array($ekskul)){ ?>
                      <option value="<?php echo $r[idEkskul]; ?>" <?php if($r[idEkskul] == get_ekstrakurikuler($tahun,$kelas,'pts',$s[id],'id_ekskul')){ echo "selected";};?>><?php echo $r[ekskul]; ?></option>
                  <?php } ?>
                 </select>
              </td>
	        		<td><input type="text" name="keterangan" onchange="update(<?=$s[id]?>,'keterangan',this,<?=$tahun?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_ekstrakurikuler($tahun,$kelas,'pts',$s[id],'keterangan');?>" style="width: 95%;"></td>
        		</tr>
        	<?php $no++;} ?>
            </tbody>
        </table>
      </div>
      </div>
      </div>
    </div>

<?php } ?>

<?php }else{ ?>
<?php 
	$tahun = $_GET['tahun'];
?>
<div style="padding: 40px">
	<a href="<?=$aksi?>?module=n_ekstrakurikuler" class="button"> Kembali</a>

	<p>Kelas:</p>

	<div class="btn-group" style="width:100%">
		<?php $kelas = mysql_query("SELECT k.id_kelas,p.nama_kelas from walikelas p, kelas k where p.nama_kelas=k.nama_kelas and p.nip='".$_SESSION[nip]."'"); 
		$i = 1;
			while($k=mysql_fetch_array($kelas)){
				if($i == 5){
					echo '</div><br><div class="btn-group" style="width:100%">';
				}
		?>
	  		<button onclick="location.href='<?=$aksi?>?module=n_ekstrakurikuler&tahun=<?=$tahun?>&kelas=<?=$k[id_kelas]?>&ujian=pts'" style="width:25%">Kelas<h2 style="color: white;"><?=$k[nama_kelas]?></h2></button>

	  	<?php $i++;} ?>
	</div>
</div>
<?php } ?>
<?php } ?>

<script type="text/javascript">
	function change_ujian(tahun,kelas,ujian){
		location.href='<?=$aksi?>?module=n_ekstrakurikuler&tahun='+tahun+'&kelas='+kelas+'&ujian='+ujian.value;
	}

	function update(siswa,tipe,nilai,tahun,kelas,ujian){
        $.ajax({
            type: 'GET',
            url: 'modul/mod_nilai/aksi_nilai_ekstrakurikuler.php',
            data: 'tahun='+tahun+'&kelas='+kelas+'&ujian='+ujian+'&tipe='+tipe+'&nilai='+nilai.value+'&siswa='+siswa,
            success: function (html) { 
            }
        });
    }
</script>
