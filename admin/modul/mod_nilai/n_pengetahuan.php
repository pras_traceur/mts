<style>
.btn-group button {
  background-color: #4CAF50; 
  border: 1px solid green; 
  color: white; 
  padding: 10px 24px;
  cursor: pointer; 
  float: left; 
  height:100px;
}

.btn-group:after {
  content: "";
  clear: both;
  display: table;
}

.btn-group button:not(:last-child) {
  border-right: none; 
}

.btn-group button:hover {
  background-color: #3e8e41;
}
</style>

<!-- =============================================================================================================== -->
<center><h1>Nilai Pengetahuan</h1></center>
<br>

<?php if(!$_GET['tahun']){ ?>
<div style="padding: 40px">
	<p>Tahun Semester:</p>

	<div class="btn-group" style="width:100%">
		<?php $tahun = mysql_query("SELECT * FROM tahun"); 
		$i = 1;
			while($t=mysql_fetch_array($tahun)){
				if($i == 5){
					echo '</div><br><div class="btn-group" style="width:100%">';
				}
		?>
	  		<button onclick="location.href='<?=$aksi?>?module=n_pengetahuan&tahun=<?=$t[id_tahun]?>'" style="width:25%">Semester <?=$t[semester]?><h2 style="color: white;"><?=$t[tahun]?></h2></button>

	  	<?php $i++;} ?>
	</div>
</div>
<?php }else{ ?>

<?php if(!$_GET['mapel']){ ?>

<?php $tahun = $_GET['tahun'];?>
<div style="padding: 40px">
	<a href="<?=$aksi?>?module=n_pengetahuan" class="button"> Kembali</a>

	<p>Mata Pelajaran:</p>

	<div class="btn-group" style="width:100%">
		<?php $mapel = mysql_query("SELECT distinct m.id_mapel,m.nama_mapel FROM pengampu p,guru g,mapel m where g.nip=p.nip and m.id_mapel=p.nama_mapel and p.nip=".$_SESSION[nip]." ORDER BY p.id_pengampu DESC"); 
		$i = 1;
			while($m=mysql_fetch_array($mapel)){
				if($i == 5){
					echo '</div><br><div class="btn-group" style="width:100%">';
				}
		?>
	  		<button onclick="location.href='<?=$aksi?>?module=n_pengetahuan&tahun=<?=$tahun?>&mapel=<?=$m[id_mapel]?>'" style="width:25%">Mata Pelajaran - <?=$m[id_mapel]?><h2 style="color: white;"><?=$m[nama_mapel]?></h2></button>

	  	<?php $i++;} ?>
	</div>
</div>

<?php }else{ ?>

<?php if($_GET['kelas']){ ?>

<?php 
	$tahun = $_GET['tahun'];
	$mapel = $_GET['mapel'];
	$kelas = $_GET['kelas'];
	$ujian = $_GET['ujian'];

	$cek=mysql_query("SELECT * FROM mapel WHERE id_mapel='".$mapel."'");
    $m=mysql_fetch_array($cek);

    function get_pengetahuan($tahun,$mapel,$kelas,$ujian,$siswa,$param){
	  $hasil=mysql_query("SELECT * FROM nilaipengetahuan WHERE id_tahun='".$tahun."' and id_mapel='".$mapel."' and id_kelas='".$kelas."' and jenis_ujian='".$ujian."' and id_siswa='".$siswa."'");
      $h=mysql_fetch_array($hasil);
	    if($h){
    		return $h[$param];
    	}else{
    		return '';
    	}
    }
?>
<div style="padding: 40px">
	<a href="<?=$aksi?>?module=n_pengetahuan&tahun=<?=$tahun?>&mapel=<?=$mapel?>" class="button"> Kembali</a>
	<select nama="ujian" onchange="change_ujian(<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,this)">
		<option value="pts" <?php if($ujian == 'pts'){ echo "selected";}?>>PTS</option>
		<option value="pas" <?php if($ujian == 'pas'){ echo "selected";}?>>PAS</option>
	</select>
</div>
<?php if($_GET['ujian'] == 'pts'){?>
	
	<div id=main-content> 
        <div class=container_12>   
        <center><?=$m[nama_mapel];?></center>
        <div class=grid_12> 
              <div class=block-border> 
              <div class=block-header> 
              <h1>Nilai PTS <?=$m[nama_mapel];?></h1>
              <span></span> 
              </div> 
               <div class='block-content'>
        		  
            <table id='table-example' class='table'>	    
            <thead>
            	<th>No</th>
            	<th>Nama</th>
        		<th>3.1</th>
        		<th>3.2</th>  
        		<th>3.3</th>  
        		<th>3.4</th>  
        		<th>3.5</th>  
        		<th>3.6</th>  
        		<th>3.7</th>  
        		<th>3.8</th>  
        		<th>3.9</th>  
        		<th>3.10</th>  
        		<th>3.11</th>  
        		<th>3.12</th>  
        		<th>3.13</th>    
            </thead>
            <tbody>
            <?php 
			$no = 1;
            
            $siswa = mysql_query("select s.id,s.nama_siswa from siswa s,kelompok k where k.id_siswa=s.id and k.id_kelas='".$kelas."' and k.id_tahun='".$tahun."'"); 

            	$no = 1;
				while($s=mysql_fetch_array($siswa)){ ?>
            	<tr class=gradeX>
	        		<td><?=$no?></td>
	        		<td><?=$s[nama_siswa]?></td>
	        		<td><input type="number" name="n1" onchange="update(<?=$s[id]?>,'n1',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pts',$s[id],'n1');?>"></td>
	        		<td><input type="number" name="n2" onchange="update(<?=$s[id]?>,'n2',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pts',$s[id],'n2');?>"></td>
	        		<td><input type="number" name="n3" onchange="update(<?=$s[id]?>,'n3',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pts',$s[id],'n3');?>"></td>
	        		<td><input type="number" name="n4" onchange="update(<?=$s[id]?>,'n4',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pts',$s[id],'n4');?>"></td>
	        		<td><input type="number" name="n5" onchange="update(<?=$s[id]?>,'n5',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pts',$s[id],'n5');?>"></td>
	        		<td><input type="number" name="n6" onchange="update(<?=$s[id]?>,'n6',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pts',$s[id],'n6');?>"></td>
	        		<td><input type="number" name="n7" onchange="update(<?=$s[id]?>,'n7',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pts',$s[id],'n7');?>"></td>
	        		<td><input type="number" name="n8" onchange="update(<?=$s[id]?>,'n8',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pts',$s[id],'n8');?>"></td>
	        		<td><input type="number" name="n9" onchange="update(<?=$s[id]?>,'n9',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pts',$s[id],'n9');?>"></td>
	        		<td><input type="number" name="n10" onchange="update(<?=$s[id]?>,'n10',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pts',$s[id],'n10');?>"></td>
	        		<td><input type="number" name="n11" onchange="update(<?=$s[id]?>,'n11',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pts',$s[id],'n11');?>"></td>
	        		<td><input type="number" name="n12" onchange="update(<?=$s[id]?>,'n12',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pts',$s[id],'n12');?>"></td>
	        		<td><input type="number" name="n13" onchange="update(<?=$s[id]?>,'n13',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pts',$s[id],'n13');?>"></td>
        		</tr>
        	<?php $no++;} ?>
            </tbody>
        </table>
      </div>
      </div>
      </div>
    </div>
    </div>
    
<?php }else{ ?>
	
	<div id=main-content> 
        <div class=container_12>   
        <center><?=$m[nama_mapel];?></center>
        <div class=grid_12> 
              <div class=block-border> 
              <div class=block-header> 
              <h1>Nilai PAS <?=$m[nama_mapel];?></h1>
              <span></span> 
              </div> 
               <div class='block-content'>
        		  
            <table id='table-example' class='table'>	    
            <thead>
            	<th>No</th>
            	<th>Nama</th>
        		<th>3.1</th>
        		<th>3.2</th>  
        		<th>3.3</th>  
        		<th>3.4</th>  
        		<th>3.5</th>  
        		<th>3.6</th>  
        		<th>3.7</th>  
        		<th>3.8</th>  
        		<th>3.9</th>  
        		<th>3.10</th>  
        		<th>3.11</th>  
        		<th>3.12</th>  
        		<th>3.13</th>    
            </thead>
            <tbody>
            <?php 
			$no = 1;
            
            $siswa = mysql_query("select s.id,s.nama_siswa from siswa s,kelompok k where k.id_siswa=s.id and k.id_kelas='".$kelas."' and k.id_tahun='".$tahun."'"); 

            	$no = 1;
				while($s=mysql_fetch_array($siswa)){ ?>
            	<tr class=gradeX>
	        		<td><?=$no?></td>
	        		<td><?=$s[nama_siswa]?></td>
	        		<td><input type="number" name="n1" onchange="update(<?=$s[id]?>,'n1',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pas',$s[id],'n1');?>"></td>
	        		<td><input type="number" name="n2" onchange="update(<?=$s[id]?>,'n2',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pas',$s[id],'n2');?>"></td>
	        		<td><input type="number" name="n3" onchange="update(<?=$s[id]?>,'n3',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pas',$s[id],'n3');?>"></td>
	        		<td><input type="number" name="n4" onchange="update(<?=$s[id]?>,'n4',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pas',$s[id],'n4');?>"></td>
	        		<td><input type="number" name="n5" onchange="update(<?=$s[id]?>,'n5',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pas',$s[id],'n5');?>"></td>
	        		<td><input type="number" name="n6" onchange="update(<?=$s[id]?>,'n6',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pas',$s[id],'n6');?>"></td>
	        		<td><input type="number" name="n7" onchange="update(<?=$s[id]?>,'n7',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pas',$s[id],'n7');?>"></td>
	        		<td><input type="number" name="n8" onchange="update(<?=$s[id]?>,'n8',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pas',$s[id],'n8');?>"></td>
	        		<td><input type="number" name="n9" onchange="update(<?=$s[id]?>,'n9',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pas',$s[id],'n9');?>"></td>
	        		<td><input type="number" name="n10" onchange="update(<?=$s[id]?>,'n10',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pas',$s[id],'n10');?>"></td>
	        		<td><input type="number" name="n11" onchange="update(<?=$s[id]?>,'n11',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pas',$s[id],'n11');?>"></td>
	        		<td><input type="number" name="n12" onchange="update(<?=$s[id]?>,'n12',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pas',$s[id],'n12');?>"></td>
	        		<td><input type="number" name="n13" onchange="update(<?=$s[id]?>,'n13',this,<?=$tahun?>,<?=$mapel?>,<?=$kelas?>,'<?=$ujian?>');" value="<?php echo get_pengetahuan($tahun,$mapel,$kelas,'pas',$s[id],'n13');?>"></td>
        		</tr>
        	<?php $no++;} ?>
            </tbody>
        </table>
      </div>
      </div>
      </div>
    </div>
    </div>
<?php } ?>

<?php }else{ ?>
<?php 
	$tahun = $_GET['tahun'];
    $mapel = $_GET['mapel'];
?>
<div style="padding: 40px">
	<a href="<?=$aksi?>?module=n_pengetahuan&tahun=<?=$tahun?>" class="button"> Kembali</a>

	<p>Kelas:</p>

	<div class="btn-group" style="width:100%">
		<?php $kelas = mysql_query("SELECT k.id_kelas,p.nama_kelas from pengampu p, kelas k where p.nama_kelas=k.nama_kelas and p.nip='".$_SESSION[nip]."' group by k.id_kelas"); 
		$i = 1;
			while($k=mysql_fetch_array($kelas)){
				if($i == 5){
					echo '</div><br><div class="btn-group" style="width:100%">';
				}
		?>
	  		<button onclick="location.href='<?=$aksi?>?module=n_pengetahuan&tahun=<?=$tahun?>&mapel=<?=$mapel?>&kelas=<?=$k[id_kelas]?>&ujian=pts'" style="width:25%">Kelas<h2 style="color: white;"><?=$k[nama_kelas]?></h2></button>

	  	<?php $i++;} ?>
	</div>
</div>
<?php } ?>
<?php } ?>
<?php } ?>

<script type="text/javascript">
	function change_ujian(tahun,mapel,kelas,ujian){
		location.href='<?=$aksi?>?module=n_pengetahuan&tahun='+tahun+'&mapel='+mapel+'&kelas='+kelas+'&ujian='+ujian.value;
	}

	function update(siswa,tipe,nilai,tahun,mapel,kelas,ujian){
        $.ajax({
            type: 'GET',
            url: 'modul/mod_nilai/aksi_nilai_pengetahuan.php',
            data: 'tahun='+tahun+'&mapel='+mapel+'&kelas='+kelas+'&ujian='+ujian+'&tipe='+tipe+'&nilai='+nilai.value+'&siswa='+siswa,
            success: function (html) { 
            }
        });
    }
</script>
