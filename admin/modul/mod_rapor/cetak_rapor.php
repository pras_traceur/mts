<script>
function goBack(parameter) {
  alert("Lengkapi Nilai "+parameter+" Terlebih Dahulu");
  window.history.back();
}
</script>
<?php

include "../../../config/koneksi.php";

$id_tahun = @$_GET[id_tahun];
$id_kelas = @$_GET[id_kelas];
$id_siswa = @$_GET[id_siswa];

//query
$identitas = mysql_query("SELECT * FROM `identitas` where id_identitas=1");
$i=mysql_fetch_array($identitas);

$kelas = mysql_query("SELECT * FROM `kelas` where id_kelas='$id_kelas'");
$k=mysql_fetch_array($kelas);

$tahun = mysql_query("SELECT * FROM `tahun` where id_tahun='$id_tahun'");
$t=mysql_fetch_array($tahun);

$siswa = mysql_query("SELECT * FROM `siswa` where id='$id_siswa'");
$s=mysql_fetch_array($siswa);

//Spiritual
$nilaispiritual = mysql_query("SELECT * FROM `nilaispiritual` where id_tahun='$id_tahun' and id_kelas='$id_kelas' and id_siswa='$id_siswa'");
$nspiritual=mysql_fetch_array($nilaispiritual);

function getspiritual($param,$kategori){
	$getspiritual = mysql_query("SELECT * FROM `spiritual` where predikat='$param' and kategori='$kategori'");
	$gspiritual=mysql_fetch_array($getspiritual);
	if(!$gspiritual){
		echo '<script type="text/javascript">goBack("Spiritual")</script>';
	}
	return $gspiritual['deskripsi'];
}

//Sosial
$nilaisosial = mysql_query("SELECT * FROM `nilaisosial` where id_tahun='$id_tahun' and id_kelas='$id_kelas' and id_siswa='$id_siswa'");
$nsosial=mysql_fetch_array($nilaisosial);

function getsosial($param,$kategori){
	$getsosial = mysql_query("SELECT * FROM `sosial` where predikat='$param' and kategori='$kategori'");
	$gsosial=mysql_fetch_array($getsosial);
	if(!$gsosial){
		echo '<script type="text/javascript">goBack("Sosial")</script>';
	}
	return $gsosial['deskripsi'];
}

//Nilai Pengetahuan
$nilai1 = mysql_query("SELECT distinct m.id_mapel,m.nama_mapel from mapel m, kelas k, pengampu p where p.nama_mapel = m.id_mapel and p.nama_kelas=k.nama_kelas and k.id_kelas='".$id_kelas."'");

$total_nilai1 = mysql_num_rows($nilai1);

//ambil nilai pengetahuan KI-3
function get_pengetahuan($id_siswa,$id_mapel,$id_kelas,$id_tahun){
	$nh = 0;
	for($z=1;$z<7;$z++){
		$zharian = mysql_query("SELECT IFNULL(n".$z.", 0) as total FROM `nilaiharian` where id_tahun='".$id_tahun."' and id_siswa='".$id_siswa."' and id_mapel='".$id_mapel."' and id_kelas='".$id_kelas."'");
		$harian=mysql_fetch_array($zharian);
		if($harian['total'] != 0){
			$nh++;
		}
	}

	$get_harian = mysql_query("SELECT (IFNULL(n1, 0)+IFNULL(n2, 0)+IFNULL(n3, 0)+IFNULL(n4, 0)+IFNULL(n5, 0)+IFNULL(n6, 0))/".$nh." as total FROM `nilaiharian` where id_tahun='".$id_tahun."' and id_siswa='".$id_siswa."' and id_mapel='".$id_mapel."' and id_kelas='".$id_kelas."'");

	$npts = 0;
	for($z=1;$z<14;$z++){
		$zpts = mysql_query("SELECT IFNULL(n".$z.", 0) as total FROM `nilaipengetahuan` where id_tahun='".$id_tahun."' and id_mapel='".$id_mapel."' and id_kelas='".$id_kelas."' and id_siswa='".$id_siswa."' and jenis_ujian='pts'");
		$pts=mysql_fetch_array($zpts);
		if($pts['total'] != 0){
			$npts++;
		}
	}

	$get_pts = mysql_query("SELECT (IFNULL(n1, 0)+IFNULL(n2, 0)+IFNULL(n3, 0)+IFNULL(n4, 0)+IFNULL(n5, 0)+IFNULL(n6, 0)+IFNULL(n7, 0)+IFNULL(n8, 0)+IFNULL(n9, 0)+IFNULL(n10, 0)+IFNULL(n11, 0)+IFNULL(n12, 0)+IFNULL(n3, 0))/".$npts." as total FROM `nilaipengetahuan` where id_tahun='".$id_tahun."' and id_mapel='".$id_mapel."' and id_kelas='".$id_kelas."' and id_siswa='".$id_siswa."' and jenis_ujian='pts'");

	$npas = 0;
	for($z=1;$z<14;$z++){
		$zpas = mysql_query("SELECT IFNULL(n".$z.", 0) as total FROM `nilaipengetahuan` where id_tahun='".$id_tahun."' and id_mapel='".$id_mapel."' and id_kelas='".$id_kelas."' and id_siswa='".$id_siswa."' and jenis_ujian='pas'");
		$pas=mysql_fetch_array($zpas);
		if($pas['total'] != 0){
			$npas++;
		}
	}
	
	$get_pas = mysql_query("SELECT (IFNULL(n1, 0)+IFNULL(n2, 0)+IFNULL(n3, 0)+IFNULL(n4, 0)+IFNULL(n5, 0)+IFNULL(n6, 0)+IFNULL(n7, 0)+IFNULL(n8, 0)+IFNULL(n9, 0)+IFNULL(n10, 0)+IFNULL(n11, 0)+IFNULL(n12, 0)+IFNULL(n3, 0))/".$npas." as total FROM `nilaipengetahuan` where id_tahun='".$id_tahun."' and id_mapel='".$id_mapel."' and id_kelas='".$id_kelas."' and id_siswa='".$id_siswa."' and jenis_ujian='pas'");

	$gharian=mysql_fetch_array($get_harian);
	if(!$gharian){
		echo '<script type="text/javascript">goBack("Pengetahuan Harian")</script>';
	}
	$gpts=mysql_fetch_array($get_pts);
	if(!$gpts){
		echo '<script type="text/javascript">goBack("Pengetahuan PTS")</script>';
	}
	$gpas=mysql_fetch_array($get_pas);
	if(!$gpas){
		echo '<script type="text/javascript">goBack("Pengetahuan PAS")</script>';
	}

	$total = ((2*$gharian['total']) + $gpts['total'] + $gpas['total'])/4;

	if($total){
		return (int)$total;
	}else{
		return 0;
	}
}

//Nilai Keterampilan
$nilai2 = mysql_query("SELECT distinct m.id_mapel,m.nama_mapel from mapel m, kelas k, pengampu p where p.nama_mapel = m.id_mapel and p.nama_kelas=k.nama_kelas and k.id_kelas='".$id_kelas."'");

//$total_nilai2 = mysql_num_rows($nilai2);
//ambil nilai keterampilan KI-4
function get_keterampilan($id_siswa,$id_mapel,$id_kelas,$id_tahun){
	$nh = 0;
	for($z=1;$z<7;$z++){
		$zharian = mysql_query("SELECT IFNULL(n".$z.", 0) as total FROM `nilaiharian` where id_tahun='".$id_tahun."' and id_siswa='".$id_siswa."' and id_mapel='".$id_mapel."' and id_kelas='".$id_kelas."'");
		$harian=mysql_fetch_array($zharian);
		if($harian['total'] != 0){
			$nh++;
		}
	}

	$get_harian = mysql_query("SELECT (IFNULL(n1, 0)+IFNULL(n2, 0)+IFNULL(n3, 0)+IFNULL(n4, 0)+IFNULL(n5, 0)+IFNULL(n6, 0))/".$nh." as total FROM `nilaiharian` where id_tahun='".$id_tahun."' and id_siswa='".$id_siswa."' and id_mapel='".$id_mapel."' and id_kelas='".$id_kelas."'");

	$npts = 0;
	for($z=1;$z<14;$z++){
		$zpts = mysql_query("SELECT IFNULL(n".$z.", 0) as total FROM `nilaiketerampilan` where id_tahun='".$id_tahun."' and id_mapel='".$id_mapel."' and id_kelas='".$id_kelas."' and id_siswa='".$id_siswa."' and jenis_ujian='pts'");
		$pts=mysql_fetch_array($zpts);
		if($pts['total'] != 0){
			$npts++;
		}
	}

	$get_pts = mysql_query("SELECT (IFNULL(n1, 0)+IFNULL(n2, 0)+IFNULL(n3, 0)+IFNULL(n4, 0)+IFNULL(n5, 0)+IFNULL(n6, 0)+IFNULL(n7, 0)+IFNULL(n8, 0)+IFNULL(n9, 0)+IFNULL(n10, 0)+IFNULL(n11, 0)+IFNULL(n12, 0)+IFNULL(n3, 0))/".$npts." as total FROM `nilaiketerampilan` where id_tahun='".$id_tahun."' and id_mapel='".$id_mapel."' and id_kelas='".$id_kelas."' and id_siswa='".$id_siswa."' and jenis_ujian='pts'");

	$npas = 0;
	for($z=1;$z<14;$z++){
		$zpas = mysql_query("SELECT IFNULL(n".$z.", 0) as total FROM `nilaiketerampilan` where id_tahun='".$id_tahun."' and id_mapel='".$id_mapel."' and id_kelas='".$id_kelas."' and id_siswa='".$id_siswa."' and jenis_ujian='pas'");
		$pas=mysql_fetch_array($zpas);
		if($pas['total'] != 0){
			$npas++;
		}
	}

	$get_pas = mysql_query("SELECT (IFNULL(n1, 0)+IFNULL(n2, 0)+IFNULL(n3, 0)+IFNULL(n4, 0)+IFNULL(n5, 0)+IFNULL(n6, 0)+IFNULL(n7, 0)+IFNULL(n8, 0)+IFNULL(n9, 0)+IFNULL(n10, 0)+IFNULL(n11, 0)+IFNULL(n12, 0)+IFNULL(n3, 0))/".$npas." as total FROM `nilaiketerampilan` where id_tahun='".$id_tahun."' and id_mapel='".$id_mapel."' and id_kelas='".$id_kelas."' and id_siswa='".$id_siswa."' and jenis_ujian='pas'");

	$gharian=mysql_fetch_array($get_harian);
	if(!$gharian){
		echo '<script type="text/javascript">goBack("Keterampilan Harian")</script>';
	}
	$gpts=mysql_fetch_array($get_pts);
	if(!$gpts){
		echo '<script type="text/javascript">goBack("Keterampilan PTS")</script>';
	}
	$gpas=mysql_fetch_array($get_pas);
	if(!$gpas){
		echo '<script type="text/javascript">goBack("Keterampilan PAS")</script>';
	}

	$total = ((2*$gharian['total']) + $gpts['total'] + $gpas['total'])/4;

	if($total){
		return (int)$total;
	}else{
		return 0;
	}
}

//ekskul
$nilaiekskul = mysql_query("SELECT e.ekskul,ne.keterangan FROM `nilaiekskul` ne,ekstrakurikuler e where ne.id_ekskul=e.idEkskul and ne.id_tahun='$id_tahun' and ne.id_kelas='$id_kelas' and ne.id_siswa='$id_siswa'");
$nekskul=mysql_fetch_array($nilaiekskul);

//ketidakhadiran
$nilaiketidakhadiran = mysql_query("SELECT * FROM `nilaiketidakhadiran` where id_tahun='$id_tahun' and id_kelas='$id_kelas' and id_siswa='$id_siswa'");
$nketidakhadiran=mysql_fetch_array($nilaiketidakhadiran);

//catatan
$nilaicatatan = mysql_query("SELECT * FROM `catatan` where id_tahun='$id_tahun' and id_kelas='$id_kelas' and id_siswa='$id_siswa'");
$ncatatan=mysql_fetch_array($nilaicatatan);
	
//predikat
function predikat($param){
	if(($param) <= 100 && ($param) >= 90 ){ return 'A'; }
	else if(($param) < 90 && ($param) >= 80 ){ return 'B';}
	else if(($param) < 80 && ($param) >= 70 ){ return 'C';}
	else if(($param) < 70 && ($param) > 0 ){ return 'D';}
	}

//Deskripsi
function get_deskripsi($id_mapel,$nama_kelas){
	$des = mysql_query("SELECT * FROM `pengetahuan` where id_mapel like '".$id_mapel."' and nama_kelas like '".$nama_kelas."'");
 	$des = mysql_fetch_array($des);
 	$deskripsi =$des['deskripsi31'].",".$des['deskripsi313'];

	if($des['deskripsi31'] || $des['deskripsi313']){
		return $deskripsi;
	}else{
		return "-";
	}
}


// TTD
$qttd_wali = mysql_query("SELECT g.nip,g.nama_guru FROM `guru` g,walikelas w,kelas k WHERE g.nip = w.nip AND w.nama_kelas = k.nama_kelas AND k.id_kelas='".$id_kelas."' order by w.id_wali desc limit 1 ");
$ttd_wali=mysql_fetch_array($qttd_wali);

$qttd_kepsek = mysql_query("SELECT kepsek,nip_kepsek FROM `identitas`");
$ttd_kepsek=mysql_fetch_array($qttd_kepsek);

$qttd_ortu = mysql_query("SELECT namaAyah,namaIbu,namaWali FROM `siswa` WHERE id='".$id_siswa."'");
$ttd_ortu=mysql_fetch_array($qttd_ortu);


$nama_file = "raport.doc";

//header("Content-Type: application/vnd.ms-word");
//header("Expires: 0");
//header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
//header("Content-disposition: attachment; filename=".$nama_file);

?>

<div id="cetak_surat">
<center><h2>PENCAPAIAN KOMPETENSI PESERTA DIDIK</h2></center>
<br><br>
	
<div style="padding-left: 40px;padding-right: 40px">
<table border="0">
	<tr>
		<td width="300px">Nama Sekolah	: <?=$i['nama'];?></td>
		<td width="300px">Kelas : <?=$k['nama_kelas']?></td>
	</tr>
	<tr>
		<td width="250px">Alamat	: <?=$i['alamat'];?></td>
		<td>Semester : <?=$t['semester']?><br>
			Tahun Ajaran : <?=$t['tahun']?>
		</td>
	</tr>
	<tr>
		<td colspan="2">Nama Siswa: <?=$s['nama_siswa']?></td>
	</tr>
	<tr>
		<td colspan="2">Nomor Induk Siswa : <?=$s['nis']?></td>
	</tr>
</table>
</div>

<br>
<b>A. Sikap</b><br>
<b>1. Sikap Spiritual</b>
<table border="1">
	<tr align="center" bgcolor="#908c8c">
		<td width="250px">
			<b>Predikat</b>
		</td>
		<td width="350px">
			<b>Deskripsi</b>
		</td>
	</tr>
	<tr height="100px" align="center">
		<td>Baik</td>
		<td><?=getspiritual($nspiritual['n1'],'Berdoa')?>,<br><?=getspiritual($nspiritual['n2'],'Baca Al Quran')?>,<br><?=getspiritual($nspiritual['n3'],'Salam')?></td>
	</tr>
</table>
<br>
<b>2. Sikap Sosial</b>
<table border="1">
	<tr align="center" bgcolor="#908c8c">
		<td width="250px">
			<b>Predikat</b>
		</td>
		<td width="350px">
			<b>Deskripsi</b>
		</td>
	</tr>
	<tr height="100px" align="center">
		<td>Baik</td>
		<td>
			<?=getsosial($nsosial['n1'],'Jujur')?>,<br>
			<?=getsosial($nsosial['n2'],'Disiplin')?>,<br>
			<?=getsosial($nsosial['n3'],'Tanggung Jawab')?>,<br>
			<?=getsosial($nsosial['n4'],'Toleransi ')?>,<br>
			<?=getsosial($nsosial['n5'],'Gotong Royong ')?>,<br>
			<?=getsosial($nsosial['n6'],'Santun')?>,<br>
			<?=getsosial($nsosial['n7'],'Percaya diri ')?>
		</td>
	</tr>
</table>
<br><br>
<b>B. Pengetahuan Dan Keterampilan</b><br>
Ketuntasan Belajar Minimal : <b>70</b><br>
<b>1. Pengetahuan</b>
<table border="1">
	<tr align="center" bgcolor="#908c8c">
		<td width="10px">
		<b>No</b>
		</td>
		<td width="90px">
			<b>Mata Pelajaran</b>
		</td>
		<td width="50px">
			<b>Angka</b>
		</td>
		<td width="50px">
			<b>Predikat</b>
		</td>
		<td width="400px">
			<b>Deskripsi</b>
		</td>
	</tr>
	<?php $no1=1;while($n1=mysql_fetch_array($nilai1)){ if($n1){?>
		<tr align="center">
		<td><?=$no1?></td>
		<td><?=$n1['nama_mapel']?></td>
		<td><?=get_pengetahuan($id_siswa,$n1['id_mapel'],$id_kelas,$id_tahun)?></td>
		<td><?=predikat(get_pengetahuan($id_siswa,$n1['id_mapel'],$id_kelas,$id_tahun),$total_nilai1)?></td>
		<td><?=get_deskripsi($n1['id_mapel'],$k['nama_kelas'])?></td>
		</tr>
	<?php $no1++;}}?>
</table>
<br>
<b>2. Keterampilan</b>
<table border="1">
	<tr align="center" bgcolor="#908c8c">
		<td width="10px">
			<b>No</b>
		</td>
		<td width="90px">
			<b>Mata Pelajaran</b>
		</td>
		<td width="50px">
			<b>Angka</b>
		</td>
		<td width="50px">
			<b>Predikat</b>
		</td>
		<td width="400px">
			<b>Deskripsi</b>
		</td>
	</tr>
		<?php $no2=1;while($n2=mysql_fetch_array($nilai2)){ if($n2){?>
		<tr align="center">
		<td><?=$no2?></td>
		<td><?=$n2['nama_mapel']?></td>
		<td><?=get_keterampilan($id_siswa,$n2['id_mapel'],$id_kelas,$id_tahun)?></td>
		<td><?=predikat(get_keterampilan($id_siswa,$n2['id_mapel'],$id_kelas,$id_tahun))?></td>
		<td><?=get_deskripsi($n2['id_mapel'],$k['nama_kelas'])?></td>
		</tr>
		<?php $no2++;}}?>
</table>
<br><br>
<b>C. Ekstrakurikuler</b><br>
<table border="1">
	<tr align="center" bgcolor="#908c8c">
		<td width="10px">
			<b>No</b>
		</td>
		<td width="190px">
			<b>Kegiatan Ekstrakurikuler</b>
		</td>
		<td width="400px">
			<b>Keterangan</b>
		</td>
	</tr>
	<tr align="center">
		<td>1</td>
		<td><?=$nekskul['ekskul']?></td>
		<td><?=$nekskul['keterangan']?></td>
	</tr>
</table>
<br><br>
<b>D. Ketidakhadiran</b><br>
<table border="1">
	<tr>
		<td width="200px">
			<b>Sakit :</b>
		</td>
		<td width="100px">
			<b><?php if($nketidakhadiran['n1']){ echo $nketidakhadiran['n1'];}else{ echo "0";}?> hari</b>
		</td>
	</tr>
	<tr>
		<td width="200px">
			<b>Izin :</b>
		</td>
		<td width="100px">
			<b><?php if($nketidakhadiran['n2']){ echo $nketidakhadiran['n2'];}else{ echo "0";}?> hari</b>
		</td>
	</tr>
	<tr>
		<td width="200px">
			<b>Tanpa Keterangan :</b>
		</td>
		<td width="100px">
			<b><?php if($nketidakhadiran['n3']){ echo $nketidakhadiran['n3'];}else{ echo "0";}?> hari</b>
		</td>
	</tr>
</table>
<?php if($t['semester'] == 'Genap' || $t['semester'] == 'genap'){?>
<br>
E. Catatan Wali Kelas
<table border="1">
	<tr>
		<td>
			<?=$ncatatan['keterangan']?>
		</td>
	</tr>
</table>
<?php } ?>
<br><br><br>
<table border="0">
	<tr>
		<td width="200px">
			Mengetahui<br>
			Orang Tua/Wali
		</td>
		<td width="200px">
			
		</td>
		<td width="200px">
			Wali Kelas
		</td>
	</tr>
</table>
<br><br><br>
<table border="0">
	<tr>
		<td width="200px">
			<u><b>
			<?php 
			if(!$ttd_ortu['namaAyah']){
				if(!$ttd_ortu['namaIbu']){
					if(!$ttd_ortu['namaWali']){
						echo "-";
					}else{
						echo $ttd_ortu['namaWali'];
					}	
				}else{
					echo $ttd_ortu['namaIbu'];
				}	
			}else{
				echo $ttd_ortu['namaAyah'];
			}
			?>
			</b></u>
		</td>
		<td width="200px"></td>
		<td width="200px">
		<u><b><?=$ttd_wali['nama_guru']?></b></u>
		<br>
		NIP. <?=$ttd_wali['nip']?>
		</td>
	</tr>
</table>
<br>
<table border="0">
	<tr>
		<td width="200px">
		</td>
		<td width="200px" style="text-align: center;">
			Mengetahui<br>
			Kepala MTS.AT TAQWA
		</td>
		<td width="200px">
		</td>
	</tr>
</table>
<br><br><br>
<table border="0">
	<tr>
		<td width="200px"></td>
		<td width="200px">
		<u><b><?=$ttd_kepsek['kepsek']?></b></u>
		<br>
		NIP. <?=$ttd_kepsek['nip_kepsek']?>
		</td>
		<td width="200px"></td>
	</tr>
</table>

</div>
<script src="../../js/jquery.min.js"></script>
<script type="text/javascript">

function printContent(el){
    var restorepage = document.body.innerHTML;
    var printcontent = document.getElementById(el).innerHTML;
    document.body.innerHTML = printcontent;
    window.print();
  window.setTimeout(function() {
        document.body.innerHTML = restorepage;
    }, 1500);
}

$(document).ready(function(){
  	printContent("cetak_surat");
});
</script>