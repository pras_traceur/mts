<style>
.btn-group button {
  background-color: #4CAF50; 
  border: 1px solid green; 
  color: white; 
  padding: 10px 24px;
  cursor: pointer; 
  float: left; 
  height:100px;
}

.btn-group:after {
  content: "";
  clear: both;
  display: table;
}

.btn-group button:not(:last-child) {
  border-right: none; 
}

.btn-group button:hover {
  background-color: #3e8e41;
}
</style>

<!-- =============================================================================================================== -->
<center><h1>Rapor</h1></center>
<br>

<?php if(!$_GET['tahun']){ ?>
<div style="padding: 40px">
	<p>Tahun Semester:</p>

	<div class="btn-group" style="width:100%">
		<?php 
    if($_SESSION[leveluser] == 'admin' || $_SESSION[leveluser] == 'wali'){
      $tahun = mysql_query("SELECT * FROM tahun"); 
    }else if($_SESSION[leveluser] == 'siswa'){
      $tahun = mysql_query("SELECT * FROM tahun t,kelompok k where t.id_tahun=k.id_tahun and k.id_siswa='".$_SESSION[id_siswa]."'"); 
    }

		$i = 1;
			while($t=mysql_fetch_array($tahun)){
				if($i == 5){
					echo '</div><br><div class="btn-group" style="width:100%">';
				}
		?>
	  		<button onclick="location.href='<?=$aksi?>?module=rapor&tahun=<?=$t[id_tahun]?>'" style="width:25%">Semester <?=$t[semester]?><h2 style="color: white;"><?=$t[tahun]?></h2></button>

	  	<?php $i++;} ?>
	</div>
</div>
<?php }else{ ?>

<?php if($_GET['kelas']){ ?>

<?php 
	$tahun = $_GET['tahun'];
	$kelas = $_GET['kelas'];
	$ujian = $_GET['ujian'];
?>
<div style="padding: 40px">
  <a href="<?=$aksi?>?module=rapor&tahun=<?=$tahun?>" class="button"> Kembali</a>
</div>
<?php if($_GET['ujian']){?>
	
	<div id=main-content> 
        <div class=container_12>   
        <div class=grid_12> 
              <div class=block-border> 
              <div class=block-header> 
              <h1>Rapor</h1>
              <span></span> 
              </div> 
               <div class='block-content'>
        		  
            <table id='table-example' class='table'>	    
            <thead>
            	<th>No</th>
              <th>NIS</th>
            	<th>Nama</th>
           		<th><center>CETAK</center></th>  
            </thead>
            <tbody>
            <?php 
			      $no = 1;
            
            if($_SESSION[leveluser] == 'admin' || $_SESSION[leveluser] == 'wali'){
              $siswa = mysql_query("select s.id,s.nis,s.nama_siswa from siswa s,kelompok k where k.id_siswa=s.id and k.id_kelas='".$kelas."' and k.id_tahun='".$tahun."'"); 
            }else if($_SESSION[leveluser] == 'siswa'){
            $siswa = mysql_query("select s.id,s.nis,s.nama_siswa from siswa s,kelompok k where k.id_siswa=s.id and k.id_kelas='".$kelas."' and k.id_tahun='".$tahun."' and k.id_siswa='".$_SESSION[id_siswa]."'"); 
            }

            	$no = 1;
				while($s=mysql_fetch_array($siswa)){ ?>
            	<tr class=gradeX>
	        		<td><?=$no?></td>
              <td><?=$s[nis]?></td>
	        		<td><?=$s[nama_siswa]?></td>
              <td><center>
                    <a href='modul/mod_rapor/cetak_rapor.php?id_tahun=<?=$tahun?>&id_kelas=<?=$kelas?>&id_siswa=<?=$s[id]?>' title='Cetak' class='with-tip'>
                    <img src='img/cetak.ico' style='width:25px;'></a>
                  </center> 
                </td>
        		</tr>
        	<?php $no++;} ?>
            </tbody>
        </table>
      </div>
      </div>
      </div>
    </div>
    </div>
<?php } ?>

<?php }else{ ?>
<?php 
	$tahun = $_GET['tahun'];
?>
<div style="padding: 40px">
	<a href="<?=$aksi?>?module=rapor" class="button"> Kembali</a>

	<p>Kelas:</p>

	<div class="btn-group" style="width:100%">
		<?php 
    if($_SESSION[leveluser] == 'admin'){
      $kelas = mysql_query("SELECT * from kelas");
    }else if($_SESSION[leveluser] == 'wali'){
      $kelas = mysql_query("SELECT k.id_kelas,p.nama_kelas from walikelas p, kelas k where p.nama_kelas=k.nama_kelas and p.nip='".$_SESSION[nip]."'");
    }else if($_SESSION[leveluser] == 'siswa'){
      $kelas = mysql_query("SELECT k.id_kelas,k.nama_kelas FROM siswa s,kelas k,kelompok kl where k.id_kelas=kl.id_kelas and s.id=kl.id_siswa and s.nis='".$_SESSION[nis]."'");
    }

		$i = 1;
			while($k=mysql_fetch_array($kelas)){
				if($i == 5){
					echo '</div><br><div class="btn-group" style="width:100%">';
				}
		?>
	  		<button onclick="location.href='<?=$aksi?>?module=rapor&tahun=<?=$tahun?>&kelas=<?=$k[id_kelas]?>&ujian=pts'" style="width:25%">Kelas<h2 style="color: white;"><?=$k[nama_kelas]?></h2></button>

	  	<?php $i++;} ?>
	</div>
</div>
<?php } ?>
<?php } ?>

<script type="text/javascript">
	function change_ujian(tahun,kelas,ujian){
		location.href='<?=$aksi?>?module=rapor&tahun='+tahun+'&kelas='+kelas+'&ujian='+ujian.value;
	}

	function update(siswa,tipe,nilai,tahun,kelas,ujian){
        $.ajax({
            type: 'GET',
            url: 'modul/mod_nilai/aksi_nilai_harian.php',
            data: 'tahun='+tahun+'&kelas='+kelas+'&ujian='+ujian+'&tipe='+tipe+'&nilai='+nilai.value+'&siswa='+siswa,
            success: function (html) { 
            }
        });
    }
</script>
